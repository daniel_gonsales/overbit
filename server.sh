#!/bin/bash
path="/server";
config="logfile ${path}/nodeapp.log
logfile flush 1
log on
logtstamp after 1
logtstamp string \"[ %t: %Y-%m-%d %c:%s ]\012\"
logtstamp on";
echo "$config" > /tmp/log.conf
# screen -c /tmp/log.conf -dmSL 'App' ts-node /server/application/App.ts
ts-node ./application/App.ts
rm /tmp/log.conf
