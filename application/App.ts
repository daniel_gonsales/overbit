import * as cluster from "cluster";
import {Master} from "./Master";
import {Worker} from "./Worker";

if (cluster.isMaster) {
    new Master;
} else {
    new Worker;
}
