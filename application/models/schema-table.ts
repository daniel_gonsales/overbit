/** Describes schema table in database. */
export class SchemaTable {

    /** Name of the table in bookshelf registry. */
    name: string;

    /** Name of the table in database. */
    table: string;

    constructor(name: string, table: string) {
        this.name = name;
        this.table = table;
    }
}
