import * as crypto from "crypto";

export function respond(status:number, message:string, full?:boolean) {
    this.status(status);

    if (message) {
        if (full) {
            this.json(message);
        } else {
            this.json({
                status: status,
                worker: process.pid,
                message: message
            });
        }
    } else {
        let message:string;
        switch (status) {
            case 404:
                message = "Not Found";
                break;
            case 500:
                message = "Internal Server Error";
                break;
            case 502:
                message = "Bad Getaway";
                break;
            default:
                message = "Something went wrong";
        }

        this.type("application/json");
        this.json({
            status: status,
            worker: process.pid,
            message: 'Error ' + status + ': ' + message
        });
    }
}

export function createPassword(password:string) {
    return crypto
        .createHmac('sha256', "cluster")
        .update(password)
        .digest('hex');
}