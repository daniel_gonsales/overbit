SET FOREIGN_KEY_CHECKS = 0;
REPLACE INTO `commissions` (`id`, `rate`, `from`, `to`)
VALUES
  (1, 5, 0, 0),
  (2, 10, 1, 1),
  (3, 5, 2, 2147483647);
SET FOREIGN_KEY_CHECKS = 1;

REPLACE INTO `settings` (`name`, `value`) VALUES ('version', '0.0.5');
