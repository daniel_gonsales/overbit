ALTER TABLE `login`
	ADD COLUMN `created` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP AFTER `id`,
	ADD UNIQUE INDEX `user_wallet` (`user_wallet`);

ALTER TABLE `transactions`
	CHANGE COLUMN `description` `description` VARCHAR(255) NULL AFTER `processed`,
	ADD INDEX `description` (`description`);

REPLACE INTO `settings` (`name`, `value`) VALUES ('version', '0.0.2');