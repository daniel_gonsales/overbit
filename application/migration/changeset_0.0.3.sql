ALTER TABLE `login`
  ADD COLUMN `email` VARCHAR(255) NULL,
  ADD COLUMN `username` VARCHAR(255) NULL,
  ADD COLUMN `password` VARCHAR(255) NULL,
  ADD UNIQUE INDEX `email` (`email`) USING BTREE,
  ADD UNIQUE INDEX `username` (`username`) USING BTREE;

ALTER TABLE `transactions`
  MODIFY COLUMN `transaction_type` enum('DEPOSIT','WITHDRAWAL','VIRTUAL') NOT NULL;

REPLACE INTO `settings` (`name`, `value`) VALUES ('version', '0.0.3');
