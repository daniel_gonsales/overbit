CREATE TABLE `settings` (
  `name` VARCHAR(50) NOT NULL,
  `value` VARCHAR(255) NOT NULL,
  PRIMARY KEY (`name`),
  UNIQUE KEY `name` (`name`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE `login` (
  `id` INT(255) NOT NULL AUTO_INCREMENT,
  `user_wallet` VARCHAR(255) NOT NULL,
  `points` INT(255) NOT NULL DEFAULT '0',
  `referral` INT(255) DEFAULT NULL,
  `role` ENUM('USER','ADMIN') NULL DEFAULT 'USER',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1;

CREATE TABLE `plans` (
  `id` INT(255) NOT NULL AUTO_INCREMENT,
  `name` VARCHAR(255) NOT NULL,
  `percentage` INT(11) NOT NULL,
  `transactions` INT(11) NOT NULL,
  `interval` INT(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1;

CREATE TABLE `wallets` (
  `id` INT(255) NOT NULL AUTO_INCREMENT,
  `user_id` INT(255) NOT NULL,
  `plan_id` INT(255) NOT NULL,
  `wallet` VARCHAR(255) NOT NULL,
  PRIMARY KEY (`id`),
  INDEX `user_id` (`user_id`),
  INDEX `plan_id` (`plan_id`),
  INDEX `wallet` (`wallet`),
  CONSTRAINT `wallets_ibfk_1` FOREIGN KEY (`user_id`) REFERENCES `login` (`id`),
  CONSTRAINT `wallets_ibfk_2` FOREIGN KEY (`plan_id`) REFERENCES `plans` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1;

CREATE TABLE `transactions` (
  `id` INT(255) NOT NULL AUTO_INCREMENT,
  `created` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `process_time` TIMESTAMP NULL DEFAULT NULL,
  `transaction_type` ENUM('DEPOSIT','WITHDRAWAL') NOT NULL,
  `transaction_hash` VARCHAR(255) NOT NULL,
  `payment_id` INT(255) NULL DEFAULT 1,
  `wallet_id` INT(255) NULL DEFAULT NULL,
  `user_id` INT(255) NULL DEFAULT NULL,
  `amount` INT(255) NOT NULL,
  `confirmations` INT(255) NOT NULL,
  `processed` ENUM('Y','N') NOT NULL DEFAULT 'N',
  `description` TEXT NULL,
  PRIMARY KEY (`id`),
  INDEX `transactions_ibfk_1` (`wallet_id`),
  INDEX `transactions_ibfk_2` (`user_id`),
  CONSTRAINT `transactions_ibfk_1` FOREIGN KEY (`wallet_id`) REFERENCES `wallets` (`id`),
  CONSTRAINT `transactions_ibfk_2` FOREIGN KEY (`user_id`) REFERENCES `login` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1;

CREATE TABLE `commissions` (
  `id` INT(255) NOT NULL AUTO_INCREMENT,
  `rate` INT(11) NULL DEFAULT NULL,
  `from` INT(255) NULL DEFAULT NULL,
  `to` INT(255) NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1;

INSERT INTO `commissions` (`rate`, `from`, `to`) VALUES (10, 0, 2147483647);
INSERT INTO `plans` (`name`, `percentage`, `transactions`, `interval`) VALUES ('1% for 100 hours', 1, 100, 3600);
REPLACE INTO `settings` (`name`, `value`) VALUES ('version', '0.0.1');