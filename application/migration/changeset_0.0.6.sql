ALTER TABLE `transactions`
  ADD COLUMN `account_type` ENUM ('PRINCIPAL', 'EARNINGS') NULL AFTER `transaction_type`;

REPLACE INTO `settings` (`name`, `value`) VALUES ('version', '0.0.6');
