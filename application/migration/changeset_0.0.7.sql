ALTER TABLE `login`
  ADD COLUMN `last_earn_time` datetime NULL DEFAULT NOW() AFTER `password`;

REPLACE INTO `settings` (`name`, `value`) VALUES ('version', '0.0.7');
