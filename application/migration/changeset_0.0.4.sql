ALTER TABLE `plans`
  ADD COLUMN `min_invest` int(11) NULL AFTER `interval`,
  ADD COLUMN `max_invest` int(11) NULL AFTER `min_invest`,
  MODIFY COLUMN `percentage` decimal(11,2) NOT NULL AFTER `name`;

ALTER TABLE `wallets`
  MODIFY COLUMN `plan_id`  int(255) NULL AFTER `user_id`;

SET FOREIGN_KEY_CHECKS = 0;
REPLACE INTO `plans` (`id`, `name`, `percentage`, `transactions`, `interval`, `min_invest`, `max_invest`)
VALUES
  (1,'0.15% each hour', 0.15, 100, 3600, 100000, 249900000),
  (2,'0.17% each hour', 0.17, 100, 3600, 250000000, 499000000),
  (3,'0.19% each hour', 0.19, 100, 3600, 500000000, NULL)
;
SET FOREIGN_KEY_CHECKS = 1;

REPLACE INTO `settings` (`name`, `value`) VALUES ('version', '0.0.4');
