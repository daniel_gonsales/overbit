import * as Express from "express";
import * as Promise from "bluebird";
import * as Utils from "../../components/Utils";
import {IModule, Register, Module} from "../DefaultModule";
import {WalletModule} from "../plans/PlansModule";
import {Env} from "../../env";
import {LoginModule} from "../login/LoginModule";
const BlockTrail: any = require('blocktrail-sdk');
const AUTO_WITHDRAWAL: "yes"|"no" = "no";
const REFERRAL_LEVEL: 1|3 = 1;

@Register
export class CronService implements IModule {
    public name: string = "cron";
    public url: string = "/cron";
    public config: any;
    public db: any;
    public blocktrail: any;
    public planService: WalletModule;
    public userService: LoginModule;

    public init() {
        this.planService = Module.moduleOfType(WalletModule);
        this.userService = Module.moduleOfType(LoginModule);
    };

    public getRoute() {
        let router = Express.Router();

        router.get("", this.startCron.bind(this));

        return router;
    }

    private startCron(response: any, resolve: any) {
        let promise = Promise.resolve();
        promise = promise.then(() => this.processUsersWithoutPlan());
        promise = promise.then(() => this.processWalletsPlans());
        return promise.then(() => {
            return Promise.all([
                this.processWithdrawalTransactions(),
                this.processDepositTransactions(),
                this.processVirtualTransactions(),
            ]).then(() => {
                if (Env.lifetimeInvestments)
                    return this.updateLifetimeInvestments();
            }).then(() => {
                Utils.respond.call(resolve, 200, "OK");
            }).catch((error) => {
                console.error(error);
                Utils.respond.call(resolve, 500, "Internal Server Error");
            });
        });
    }

    private processDepositTransactions() {
        return this.db.query(`
                SELECT \`transactions\`.\`id\`, \`login\`.\`user_wallet\`, \`transactions\`.\`amount\`, \`wallets\`.\`user_id\`, \`referral\`, \`plans\`.\`percentage\`, \`plans\`.\`transactions\`, \`plans\`.\`interval\`
                FROM \`transactions\`
                LEFT JOIN \`wallets\` ON \`wallets\`.\`id\` = \`transactions\`.\`wallet_id\`
                LEFT JOIN \`login\` ON \`login\`.\`id\` = \`wallets\`.\`user_id\`
                LEFT JOIN \`plans\` ON \`plans\`.\`id\` = \`wallets\`.\`plan_id\`
                WHERE \`confirmations\` > 0 AND \`processed\` = 'N' AND \`transaction_type\` = "DEPOSIT"
            `)
            .then((result: any[]) => {
                let queue = Promise.resolve();
                let promises = [];

                if (result[0] && result[0].length) {
                    result[0].forEach((transaction: any) => {
                        let id = transaction.id;
                        let referral = transaction.referral;

                        queue = queue.then(() => {
                            // do to schedule earnings if 'forever deposit'
                            if (Env.lifetimeInvestments)
                                return;

                            // do not process process deposits with (null) plan
                            // this may occur if deposit amount is too low
                            let percentage = Number(transaction.percentage);
                            if (isNaN(percentage) || percentage <= 0) return;
                            console.info(`transactionID: ${transaction.id} percentage: ${percentage}`);

                            let values = [];
                            let amount = (transaction.amount * (percentage / 100)).toFixed(0);
                            switch (AUTO_WITHDRAWAL) {
                                case "yes":
                                    for (let i = 1, count = transaction.transactions; i <= count; i++) {
                                        const hash = `WITHDRAWAL_${transaction.user_wallet}_${id}_${i}_${amount}`;
                                        const desc = `Withdrawal ${BlockTrail.toBTC(amount)} BTC to ${transaction.user_wallet} (${i}/${count})`;
                                        values.push(`(INTERVAL ${transaction.interval * i} SECOND + now(), 'WITHDRAWAL', null, '${hash}', '${i}', '${transaction.user_id}', '${amount}', 0, 'N', '${desc}')`);
                                    }
                                    break;

                                case "no":
                                default:
                                    for (let i = 1, count = transaction.transactions; i <= count; i++) {
                                        const hash = `VIRTUAL_${transaction.user_wallet}_${id}_${i}_${amount}`;
                                        const desc = `Hourly ${BlockTrail.toBTC(amount)} BTC to ${transaction.user_wallet} (${i}/${count})`;
                                        values.push(`(INTERVAL ${transaction.interval * i} SECOND + now(), 'VIRTUAL', 'EARNINGS', '${hash}', '${i}', '${transaction.user_id}', '${amount}', 0, 'N', '${desc}')`);
                                    }
                                    break;
                            }
                            return this.db.query(`
                                    INSERT INTO \`transactions\`
                                    (\`process_time\`, \`transaction_type\`, \`account_type\`, \`transaction_hash\`, \`payment_id\`, \`user_id\`, \`amount\`, \`confirmations\`, \`processed\`, \`description\`)
                                    VALUES
                                    ${values.join(', ')}
                                `);
                        });

                        if (referral == null) {
                            queue = queue
                                .then(() => {
                                    return this.db.query(`UPDATE \`transactions\` SET \`processed\` = ?, \`process_time\` = now() WHERE \`id\` = ?`, ["Y", id]);
                                });
                        } else {
                            // Get Referals
                            switch (REFERRAL_LEVEL) {
                                case 1:
                                    // Single Referral -> Super system
                                    promises.push(this.db.query(`
                                        SELECT \`login\`.\`user_wallet\`, \`rate\`
                                        FROM \`login\`
                                        LEFT JOIN \`commissions\` ON \`commissions\`.\`from\` <= \`login\`.\`points\` AND \`commissions\`.\`from\` >= \`login\`.\`points\`
                                        LEFT JOIN \`login\` b ON b.\`id\` = \`login\`.\`id\`
                                        WHERE \`login\`.\`id\` = ?
                                        `, [referral])
                                        .then((result: any[]) => {
                                            return [{
                                                id: id,
                                                amount: transaction.amount,
                                                user: referral,
                                                wallet: result[0][0].user_wallet,
                                                rate: result[0][0].rate
                                            }]
                                        }));
                                    break;

                                case 3:
                                    // Get 3 wallets
                                    promises.push(this.db.query(`
                                        SELECT a.\`user_wallet\` as wallet1, a.\`id\` as user1, b.\`user_wallet\` as wallet2, b.\`id\` as user2, c.\`user_wallet\` as wallet3, c.\`id\` as user3 FROM \`login\` as a
                                        LEFT JOIN \`commissions\` ON \`commissions\`.\`from\` <= a.\`points\` AND \`commissions\`.\`from\` >= a.\`points\`
                                        LEFT JOIN \`login\` b ON b.\`id\` = a.\`referral\`
                                        LEFT JOIN \`login\` c ON c.\`id\` = b.\`referral\`
                                        WHERE a.\`id\` = ?
                                        `, [referral])
                                        .then((result: any[]) => {
                                            const ref = [];

                                            if (result[0][0].wallet1) {
                                                ref.push({
                                                    id: id,
                                                    amount: transaction.amount,
                                                    user: result[0][0].user1,
                                                    wallet: result[0][0].wallet1,
                                                    rate: 15
                                                });
                                            }

                                            if (result[0][0].wallet2) {
                                                ref.push({
                                                    id: id,
                                                    amount: transaction.amount,
                                                    user: result[0][0].user2,
                                                    wallet: result[0][0].wallet2,
                                                    rate: 5
                                                });
                                            }

                                            if (result[0][0].wallet3) {
                                                ref.push({
                                                    id: id,
                                                    amount: transaction.amount,
                                                    user: result[0][0].user3,
                                                    wallet: result[0][0].wallet3,
                                                    rate: 1
                                                });
                                            }

                                            return ref;
                                        }));
                                    break;
                            }
                        }
                    });
                }

                return queue
                    .then(() => {
                        if (promises && promises.length) {
                            return Promise.all(promises)
                                .then((result: any[]) => {
                                    let values: string[] = [];

                                    result.forEach((value: any, index: number) => {
                                        value && value
                                            .forEach((refer: any, index2: number) => {
                                                let total = ((refer.rate / 100) * refer.amount).toFixed(0);
                                                let hash = `REF_${Date.now()}_${index}_${index2}_${refer.wallet}_${total}`;
                                                let description = `Ref. commission ${BlockTrail.toBTC(total)} BTC to ${refer.wallet}`;
                                                switch (AUTO_WITHDRAWAL) {
                                                    case 'yes':
                                                        values.push(`(now(), 'WITHDRAWAL', null, '${hash}', ${refer.user}, ${total}, 0, 'N', '${description}')`);
                                                        break;
                                                    case 'no':
                                                    default:
                                                        values.push(`(now(), 'VIRTUAL', 'EARNINGS', '${hash}', ${refer.user}, ${total}, 0, 'N', '${description}')`);
                                                        break;
                                                }
                                            });
                                    });

                                    return this.db.query(`
	                                        INSERT INTO \`transactions\`
	                                        (\`process_time\` ,\`transaction_type\`, \`account_type\`, \`transaction_hash\`, \`user_id\`, \`amount\`, \`confirmations\`, \`processed\`, \`description\`)
	                                        VALUES
	                                        ${values.join(', ')}
	                                    `)
                                        .then(() => {
                                        	return result.reduce((prev, current) => {
                                        		var id;

                                        		if (current instanceof Array) {
                                        			id = current[0].id;
                                        		} else {
                                        			id = current.id;
                                        		}

                                        		if (prev.indexOf(id) == -1) {
                                        			prev.push(id);
                                        		}

                                        		return prev;
                                        	}, []);
                                        });
                                })
                                .then((result: any[]) => {
                                    return this.db.query(`
                                        UPDATE \`transactions\`
                                        SET
                                            \`processed\` = 'Y',
                                            \`process_time\` = now()
                                        WHERE
                                            \`id\` IN (${result.join(", ")})
                                    `);
                                });
                        }
                    });
            });
    }

    private processWithdrawalTransactions() {
        return this.db.query(`
                SELECT \`transactions\`.\`id\`, \`transactions\`.\`amount\`, \`login\`.\`user_wallet\` FROM \`transactions\`
                LEFT JOIN \`login\` ON \`login\`.\`id\` = \`transactions\`.\`user_id\`
                WHERE (\`process_time\` <= now() OR \`process_time\` IS NULL) AND \`processed\` = 'N' AND \`transaction_type\` = "WITHDRAWAL"
                AND \`user_wallet\` IS NOT NULL
            `)
            .then((result: any[]) => {
                if (result[0] && result[0].length) {
                    let payments = result[0]
                        .reduce((transactions: any[], transaction: any) => {
                            let userWallet = transaction.user_wallet;
                            transactions[userWallet] = transactions[userWallet] ?
                                transactions[userWallet] + transaction.amount :
                                transaction.amount;

                            // withdraw commision
                            transactions[userWallet] = this.applyWithdrawCommission(transactions[userWallet]);
                            return transactions;
                        }, {});
                    let ids = result[0]
                        .map((value) => value.id);

                    return Promise.resolve()
                        .then(() => {
                            return new Promise((resolve: Function, reject: Function) => {
                                this.blocktrail.initWallet(this.config.blocktrail.wallet_id, this.config.blocktrail.wallet_password, (error: any, wallet: any) => {
                                    if (error) {
                                        console.log("Can not init wallet", error);
                                        reject(error);
                                    } else {
                                        resolve(wallet);
                                    }
                                });
                            })
                        })
                        .then((wallet: any) => {
                            return new Promise((resolve: Function, reject: Function) => {
                                wallet.pay(payments, null, false, true, (<any> BlockTrail).Wallet.FEE_STRATEGY_BASE_FEE, (error: any, result: any) => {
                                    if (error) {
                                        console.log("Can't pay", error);
                                        reject(error);
                                    } else {
                                        resolve(result);
                                    }
                                })
                            })
                        })
                        .then((result: any) => {
                            return this.db.query(`
                                UPDATE \`transactions\`
                                SET
                                    \`processed\` = 'Y',
                                    \`process_time\` = now(),
                                    \`transaction_hash\` = '${result}'
                                WHERE
                                    \`id\` IN (${ids.join(', ')})
                            `);
                        })
                }
            });
    }

    private processVirtualTransactions() {
        return this.db.query(`
        UPDATE \`transactions\`
        SET \`processed\` = 'Y'
        WHERE \`transaction_type\` = 'VIRTUAL' AND \`processed\` = 'N'
        AND \`process_time\` < NOW()
        `)
    }

    public applyWithdrawCommission(value: number): number {
        // 5% withdraw commission
        return Number(value) * 0.95;
    }

    /**
     * HACK: set current time as last earn time
     * for user that doesn't have any plan configured yet.
     * this prevents from giving user extra money on first deposit
     * when deposit is made after some time on registration
     */
    private processUsersWithoutPlan() {
        return this.db.query(`
        UPDATE \`login\`
        LEFT JOIN \`wallets\` ON \`login\`.\`id\` = \`wallets\`.\`user_id\`
        SET \`login\`.\`last_earn_time\` = NOW()
        WHERE \`plan_id\` IS NULL
        `)
    }

    private processWalletsPlans(): Promise<any> {
        // select wallets
        return this.selectWalletsToUpdate()
            .then((wallets: any[]) => Promise.each(wallets, (wallet: any) => {
                // update plan of each wallet
                return this.planService.updatePlanByWalletUser(wallet.id, wallet.user_id);
            }));
    }

    private selectWalletsToUpdate(): Promise<any[]> {
        return this.db.query(`
        SELECT DISTINCT \`wallets\`.*
        FROM \`transactions\`
        LEFT JOIN \`wallets\` ON \`transactions\`.\`wallet_id\` = \`wallets\`.\`id\` 
                              OR \`transactions\`.\`user_id\` = \`wallets\`.\`user_id\`
        WHERE ((\`confirmations\` > 0 AND \`transaction_type\` = 'DEPOSIT') OR \`transaction_type\` = 'WITHDRAWAL')
        AND \`plan_select\` != 'MANUAL'
        AND \`processed\` = 'N'
        `).then(results => results[0]);
    }

    private updateLifetimeInvestments(): Promise<any> {
        return this.selectUsersHasTimeToEarn().then((users: any[]) => {
            if (users && users.length) {
                const now = new Date().getTime();
                const userValues = [];
                const transactionValues = [];
                return Promise.each(users, (user: any) => {
                    // TODO: update time when user makes deposit
                    // use 1H by default to tick accounts without deposits
                    let interval = Number(user.interval);
                    if (isNaN(interval) || interval <= 0) interval = 3600;

                    // calculate how much cycles user pass without earnings
                    let cycles = 0;
                    const last_earn_time = user.last_earn_time;
                    const date = new Date(last_earn_time);
                    const time = date.getTime();
                    while (time + (cycles + 1) * interval * 1000 < now) cycles++;

                    // exit if haven't completed any cycle
                    if (cycles < 1)
                        return;

                    // query to update user last earn time
                    const userID = user.id;
                    userValues.push(`UPDATE \`login\` SET \`last_earn_time\` = \`last_earn_time\` + INTERVAL (${cycles} * ${interval}) SECOND WHERE id=${userID};`);

                    // query to schedule transactions
                    return this.userService.queryUserBalanceSummary(userID).then((summary) => {

                        // do not process process deposits with (null) plan
                        // this may occur if deposit amount is too low
                        const percentage = Number(user.percentage);
                        if (isNaN(percentage) || percentage <= 0)
                            return;

                        // double check user has earnings
                        const earnings = percentage * cycles * summary.principal / 100;
                        if (isNaN(earnings) || earnings <= 0)
                            return;

                        const amount = earnings.toFixed(0);
                        const userWallet = user.user_wallet;
                        const hash = `VIRTUAL_${userWallet}_${userID}_${amount}`;
                        const dateUS = date.toISOString().split('.')[0].split('T').join(' ');
                        const desc = `Hourly [${dateUS}] ${BlockTrail.toBTC(summary.principal)}BTC x [${cycles} x ${interval}] x ${user.percentage}% = ${BlockTrail.toBTC(amount)} BTC to ${userWallet}`;
                        transactionValues.push(`(now(), 'VIRTUAL', 'EARNINGS', '${hash}', '${userID}', '${amount}', 0, 'N', '${desc}')`);
                    });
                }).then(() => {
                    // ToDo: better way to update multiple columns
                    if (userValues && userValues.length)
                        return Promise.all(userValues.map(query => this.db.query(query)));
                }).then(() => {
                    if (transactionValues && transactionValues.length)
                        return this.db.query(`
                        INSERT INTO \`transactions\`
                        (\`process_time\`, \`transaction_type\`, \`account_type\`, \`transaction_hash\`, \`user_id\`, \`amount\`, \`confirmations\`, \`processed\`, \`description\`)
                        VALUES
                        ${transactionValues.join(', ')}
                        `)
                });
            }
        })
    }

    private selectUsersHasTimeToEarn(): Promise<any[]> {
        return this.db.query(`
        SELECT \`login\`.\`id\`, \`login\`.\`user_wallet\`, \`plans\`.\`id\` AS plan_id, \`plans\`.\`percentage\`, \`plans\`.\`interval\`, \`last_earn_time\`
        FROM \`login\`
        LEFT JOIN \`wallets\` ON \`login\`.\`id\` = \`wallets\`.\`user_id\`
        LEFT JOIN \`plans\` ON \`wallets\`.\`plan_id\` = \`plans\`.\`id\`
        WHERE \`last_earn_time\` + INTERVAL COALESCE(\`interval\`, 3600) SECOND < NOW()
        GROUP BY \`login\`.\`id\`
        `).then(results => results[0]);
    }
}
