import * as fs from "fs";
import * as Promise from "bluebird";
import * as Express from "express";
import * as BlockTrail from "blocktrail-sdk";
import * as Utils from "../../components/Utils";
import {IModule, Register} from "../DefaultModule";

@Register
export class LoginModule implements IModule {
    public name: string = "callback";
    public url: string = "/callback";
    public config: any;
    public db: any;
    public blocktrail: any;

    public init() {
        this.blocktrail.getWebhook(this.config.blocktrail.webhook_id, (error, result) => {
            if (error) {
                console.error(error.message);
            }
        });
    };

    public getRoute() {
        let router = Express.Router();

        router.post("/event", this.parseEvent.bind(this));

        return router;
    }

    private parseEvent(request: any, response: any) {
        if (request.body && request.query["secret"] == this.config.blocktrail.apiSecret) {
            fs.appendFileSync("./callback.log", JSON.stringify(request.body) + "\r\n");

            switch (request.body["event_type"]) {
                case "address-transactions":
                    let data = request.body["data"];
                    let addresses: any[] = Object.keys(request.body["addresses"]);
                    let input: any = data["inputs"][0];

                    let transactions = [];
                    addresses
                        .forEach((wallet: string) => {
                            transactions.push(
                                this.db.query("SELECT `id` FROM `wallets` WHERE `wallet` = ?", [wallet])
                                    .then((result: any[]) => {
                                        // ToDo: Doing nothing if wallet id is incorrect
                                        // Current: Exception
                                        if (result[0] && result[0].length == 1) {
                                            return result[0][0]["id"];
                                        }
                                    })
                                    .then((wallet_id?: number) => {
                                        // ToDo: Make readable
                                        let amount = (<any> BlockTrail).toBTC(request.body["addresses"][wallet]);
                                        return this.db.query(`
                                                SELECT \`id\`
                                                FROM \`transactions\`
                                                WHERE
                                                    \`transaction_hash\` = ?
                                            `, [data.hash])
                                            .then((result: any) => {
                                                if (result[0] && result[0].length) {
                                                    return this.db.query(`
                                                        UPDATE \`transactions\`
                                                        SET
                                                            \`confirmations\` = CASE WHEN \`confirmations\` > ? THEN \`confirmations\` ELSE ? END
                                                        WHERE
                                                            \`transaction_hash\` = ?
                                                    `, [data.confirmations, data.confirmations, data.hash]);
                                                } else {
                                                    if (wallet_id && request.body["addresses"][wallet] >= 1000000) {
                                                        return this.db.query(`
                                                        INSERT INTO \`transactions\`
                                                        SET ?
                                                        ON DUPLICATE KEY UPDATE
                                                        confirmations = CASE WHEN confirmations > VALUES(confirmations) THEN confirmations ELSE VALUES(confirmations) END
                                                    `, {
                                                            transaction_type: "DEPOSIT",
                                                            transaction_hash: data.hash,
                                                            amount: request.body["addresses"][wallet],
                                                            wallet_id: wallet_id,
                                                            confirmations: data.confirmations,
                                                            description: `Deposit ${amount} BTC from ${input.address} to ${wallet}`
                                                        });
                                                    } else {
                                                        // Invalid wallet -> Ignore
                                                        return Promise.resolve();
                                                    }
                                                }
                                            });
                                    })
                            )
                        });

                    Promise.all(transactions)
                        .then(() => {
                            Utils.respond.call(response, 200, "OK");
                        })
                        .catch((error) => {
                            console.error(error);
                            Utils.respond.call(response, 500, "Internal Server Error");
                        });
                    break;
                default:
                    Utils.respond.call(response, 405, "Method Not Allowed");
            }
        } else {
            Utils.respond.call(response, 400, "Bad Request");
        }
    }
}