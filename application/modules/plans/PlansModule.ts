import * as Express from "express";
import * as Promise from "bluebird";
import * as Utils from "../../components/Utils";
import {IModule, Authorize, Register, Module} from "../DefaultModule";
import {LoginModule} from "../login/LoginModule";

@Register
export class WalletModule implements IModule {
    public name: string = "plans";
    public url: string = "/plans";
    public config: any;
    public db: any;
    public blocktrail: any;
    public userService: LoginModule;

    public init() {
        this.userService = Module.moduleOfType(LoginModule);
    };

    public getRoute() {
        let router = Express.Router();

        router.get("/list", this.getPlans.bind(this));
        router.post("/create", this.selectPlan.bind(this));

        return router;
    }

    @Authorize(["USER", "ADMIN"])
    private getPlans(request: any, response: any) {
        let user = request.session["login"];

        this.db.query(`
            SELECT \`plans\`.\`id\`, \`plans\`.\`name\`, \`plans\`.\`percentage\`, \`plans\`.\`transactions\`, \`wallet\` FROM \`plans\`
            LEFT JOIN \`wallets\` ON \`plans\`.\`id\` = \`wallets\`.\`plan_id\` AND \`wallets\`.\`user_id\` = ?
        `, [user.id])
            .then((result: any[]) => {
                Utils.respond.call(response, 200, result[0]);
            })
            .catch((error: Error) => {
                console.error(error);
                Utils.respond.call(response, 500, "Internal Server Error");
            });
    }

    @Authorize(["USER", "ADMIN"])
    private selectPlan(request: any, response: any) {
        if (request.body && request.body["plan_id"]) {
            let user = request.session["login"];

            Promise.resolve()
                .then(() => {
                    // Get plan if exists
                    return this.db.query(`
                    SELECT \`plans\`.\`id\`, \`plans\`.\`name\`, \`plans\`.\`percentage\`, \`plans\`.\`transactions\`, \`wallet\`
                    FROM \`plans\`
                    LEFT JOIN \`wallets\` ON \`plans\`.\`id\` = \`wallets\`.\`plan_id\`
                    WHERE \`wallets\`.\`user_id\` = ?
                    `, [user.id])
                })
                .then((result: any[]) => {
                    if (result[0].length == 1 && result[0][0]['wallet'] != null) {
                        return result[0][0];
                    } else {
                        // Create wallet
                        let newWallet = null;
                        return Promise.resolve()
                            .then(() => {
                                return new Promise((resolve: any, reject: any) => {
                                    return this.blocktrail.initWallet(this.config.blocktrail.wallet_id, this.config.blocktrail.wallet_password, (error: any, wallet: any) => {
                                        if (error) {
                                            reject(error);
                                        } else {
                                            resolve(wallet);
                                        }
                                    })
                                });
                            })
                            .then((wallet: any) => {
                                return new Promise((resolve, reject) => {
                                    wallet.getNewAddress((error: any, new_wallet: any) => {
                                        if (error) {
                                            reject(error);
                                        } else {
                                            newWallet = new_wallet;
                                            resolve(new_wallet);
                                        }
                                    });
                                })
                            })
                            .then((new_wallet: any) => {
                                return this.db.query(`INSERT INTO \`wallets\` (\`user_id\`, \`plan_id\`, \`wallet\`) VALUES (?, ?, ?)`, [user.id, request.body["plan_id"], new_wallet])
                            })
                            .then(() => {
                                let respond = result[0][0] || {};
                                respond["wallet"] = newWallet;
                                return respond;
                            });
                    }
                })
                .then((result: string) => {
                    Utils.respond.call(response, 200, result);
                })
                .catch((error: any) => {
                    console.error(error);
                    Utils.respond.call(response, 500, "Internal Server Error");
                });
        } else {
            Utils.respond.call(response, 400, "Bad Request");
        }
    }

    public updatePlanByWalletUser(walletID: number|string, userID: number|string) {
        // calculate principal investment of provided user
        return this.selectUserPrincipal(userID).then(principal => {
            // for wallet update plan depending on active principal
            return this.updateWalletPlan(walletID, principal);
        })
    }

    public selectUserPrincipal(userID: string|number): Promise<number> {
        return this.userService.queryUserBalanceSummary(userID)
            .then(summary => summary.principal);
    }

    private updateWalletPlan(walletID, principal): Promise<void> {
        console.log(`update wallet plan walletID: ${walletID} balance: ${principal}`);
        return this.db.query(`
        UPDATE \`wallets\`
        LEFT JOIN \`plans\` ON (? >= \`min_invest\` AND (\`max_invest\` IS NULL OR ? < \`max_invest\`))
        SET \`wallets\`.\`plan_id\` = \`plans\`.\`id\`
        WHERE \`wallets\`.\`id\` = ?
        `, [principal, principal, walletID]);
    }
}
