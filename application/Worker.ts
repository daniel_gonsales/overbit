import * as fs from 'fs';
import * as path from 'path';
import * as http from 'http';
import * as https from 'https';
import * as request from 'request';

import * as mysql from 'mysql2/promise.js';
import * as blocktrail from "blocktrail-sdk";

import  * as Promise from "bluebird";
import  * as express from "express";
import  * as expressSession from "express-session";
import  * as expressMySQLSession from "express-mysql-session";

import * as bodyParser from "body-parser";
import * as cookieParser from 'cookie-parser';

import * as Utils from "./components/Utils";
import {instance} from "./modules/DefaultModule";

export class Worker {
    public constructor() {
        this.setupListener();
    }

    private setupListener() {
        let listener = (msg:string) => {
            if (msg["config"]) {
                this.initWorker(msg["config"]);
                process.removeListener("message", listener);
            }
        };

        process.on("message", listener)
    }

    private initWorker(config:any) {
        let blocktrailClient = (<any> blocktrail).BlocktrailSDK(config.blocktrail);
        let mysqlClient = mysql.createConnection(config.database);

        Promise.all([blocktrailClient, mysqlClient])
            .then((result:any[]) => {
                let [blocktrail, connection] = result;
                let app = express();

                app.use(cookieParser());
                app.use(bodyParser.json({
                	limit: '50mb'
                }));

                let sessionStore = expressMySQLSession(expressSession);
                app.use(expressSession({
                    name: "session",
                    store: new sessionStore(config.database),
                    resave: true,
                    saveUninitialized: false,
                    secret: config.auth.secret,
                    cookie: {
                        httpOnly: true,
                        // secure: true,
                        // path: '/'
                    }
                }));

                app.use("/api", instance(config, connection, blocktrail));
                app.use(express.static(path.resolve(process.cwd(), 'public')));

                app.use((error:any, request, response, next) => {
                    // ToDo: Error logs
                    console.error(error);
                    Utils.respond.call(response, 500, "Internal Server Error");
                });

                http.createServer((req, response) => {
                	if (req.url == '/api/cron') {
                		request.get({
                			url: "https://localhost" + req.url,
  							rejectUnauthorized: false
                		}).pipe(response);
                	} else {
                        let code = (req.method === 'POST') ? 307 : 302;
                        response.writeHead(code, {
	                		"Location": "https://" + req.headers['host'] + req.url
	                	});
	    				response.end();
    				}
                }).listen(config.app.port, function () {
                    console.log("HTTP Process " + process.pid + " is listening on " + this.address().port + " port");
                });

                https.createServer({
                    key: fs.readFileSync(path.resolve(__dirname, "ssl/key.key")), 
                    cert: fs.readFileSync(path.resolve(__dirname, "ssl/cert.crt")), 
                    ca: fs.readFileSync(path.resolve(__dirname, "ssl/ca.crt"))
                }, app).listen(config.app.sslPort, function () {
                    console.log("HTTPS Process " + process.pid + " is listening on " + this.address().port + " port");
                });
            })
            .catch((error) => {
                console.log(error);
            });
    }
}