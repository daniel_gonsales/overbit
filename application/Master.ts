import * as fs from "fs";
import * as path from "path";
import * as cluster from "cluster";
import * as http from "http";
import {CronJob} from "cron";
import * as Promise from "bluebird";
import * as mysql from "mysql2/promise.js";
import * as semver from "semver";
import {Env} from "./env";

export class Master {
    private connection: any;
    private config: any;

    public constructor() {
        const env = process.env;
        this.config = {
            app: {
                title: "gatetobit",
                version: "0.0.8",
                port: env.HTTP_PORT || 80,
                sslPort: env.HTTPS_PORT || 443
            },
            auth: {
                secret: env.AUTH_SECRET || "secret"
            },
            database: {
                host: env.DB_HOST || "127.0.0.1",
                port: env.DB_PORT || "3306",
                user: env.DB_USER || "root",
                password: env.DB_PASS || "zb1024LV",
                database: env.DB_NAME || "gatetobit",
            },
			blocktrail: {
                apiKey: env.BLOCKTRAIL_API_KEY || "9efa6c042bae79acd68f11dbb9358ae9062539bd",
                apiSecret: env.BLOCKTRAIL_API_SECRET || "8517c664084f4cb0a550266eca14f8bcaa9abbfc",
                network: "BTC",
                testnet: true,
                wallet_id: env.BLOCKTRAIL_WALLET_ID || "myexplorewallet1",
                wallet_password: env.BLOCKTRAIL_WALLET_PASS || "Myexppass@sa1" ,
                webhook_id: "wallet-myexplorewallet1"
            }
        };

        this.run();
    }

    private run() {
        Promise.resolve()
            .then(() => {
                console.log("Connecting to database");
                return mysql.createConnection(this.config.database);
            })
            .then((connection: any) => {
                this.connection = connection;
                console.log("Gettings database version");
                return this.getDBVersion(connection);
            })
            .then((version?: string) => {
                console.log("Database version:", version);
                return this.makeDBUpdate(version, this.connection);
            })
            .then(() => {
                return new Promise((resolve: Function, reject: Function) => {
                    let online = 0;
                    const cpu = Env.maxCPU;
                    console.log('Master cluster setting up ' + cpu + ' workers...');

                    for (let i = 0; i < cpu; i++) {
                        cluster.fork();
                    }

                    cluster.on('online', (worker: cluster.Worker) => {
                        worker.send({
                            config: this.config
                        });

                        if (++online == cpu) {
                            resolve();
                        }
                    });

                    cluster.on('exit', function (worker: cluster.Worker, code: number, signal: string) {
                        console.log('Worker ' + worker.process.pid + ' died with code: ' + code + ', and signal: ' + signal);
                        console.log('Starting a new worker');
                        cluster.fork();
                    });
                });
            })
            .then(() => Promise.delay(5000))
            .then(() => {
                console.log("Setting up Cron");
                let lock = false;
                new CronJob('*/5 * * * * *', () => {
                    if (lock) {
                        console.log("Cron is locked");
                    } else {
                        lock = true;
                        http.get(`http://46.101.92.228/api/cron`, (response: http.IncomingMessage) => {
                            lock = false;
                            //console.log(response);
                            if (response.statusCode !== 200) {
                                console.log("Cron has failed");
                            }
                        });
                    }
                }, null, true);
            })
            .catch((error: any) => {
                console.log("Fucked up", error);
                this.connection && this.connection.end();
            })
    }

    private getDBVersion(connection) {
        return connection.query("SELECT `value` FROM `settings` WHERE `name` = 'version'")
            .then((response: any[]) => {
                let result: any = response[0];
                return result[0].value;
            })
            .catch(() => {
                return null;
            });
    }

    private makeDBUpdate(version: string, connection: any) {
        return new Promise((resolve: Function, reject: Function) => {
            let appVersion = this.config.app.version;
            if (semver.lt(version, appVersion)) {
                let files = [];
                let migrationPath = path.resolve(__dirname, "migration");

                if (fs.existsSync(migrationPath)) {
                    files = fs.readdirSync(migrationPath)
                        .filter((value: string) => fs.statSync(path.resolve(migrationPath, value)).isFile());
                }

                if (version == null) {
                    console.log("Database not initialized, setting up");
                } else {
                    console.log("Database is outdated");
                    files = files.filter((filename: string) => shouldRunSql(appVersion, version, filename));
                }

                let query = Promise.resolve();

                query = query.then(() => {
                    return connection.query("START TRANSACTION;");
                });

                files.forEach((value: string) => {
                    query = query
                        .then(() => {
                            console.log("Running " + value);

                            let resolve = Promise.resolve();
                            let file = fs.readFileSync(path.resolve(migrationPath, value), "utf8").toString()
                                .replace(/(\r\n|\n|\r)/gm, " ")
                                .replace(/\s+/g, ' ')
                                .split(";")
                                .map(Function.prototype.call, String.prototype.trim)
                                .filter((el) => el.length != 0);

                            for (let i = 0; i < file.length; i++) {
                                resolve = resolve
                                    .then(() => {
                                        return connection.query(file[i]);
                                    });
                            }

                            return resolve
                                .then(() => {
                                    console.log("Done " + value)
                                })
                                .catch((error: any) => {
                                    console.log("Failed " + value + " with ", error);
                                    reject();
                                });
                        });
                });

                query
                    .then(() => {
                        return connection.query("COMMIT;");
                    })
                    .then(() => {
                        resolve();
                    })
                    .catch((error: any) => {
                        connection.query("ROLLBACK;");
                        reject(error);
                    });
            } else {
                console.log("Database is up to date");
                resolve();
            }
        });
    }
}

function shouldRunSql(appVersion: string, dbVersion: string, filename: string) {
    // don't use version > app version
    const version = filename.substring(10, filename.length - 4);
    if (semver.gt(version, appVersion))
        return false;

    // user version > dbVersion
    return semver.gt(version, dbVersion);
}
