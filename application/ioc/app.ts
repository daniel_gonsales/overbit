import {Container, interfaces} from "inversify"
import getDecorators from "inversify-inject-decorators";
import {makeProvideDecorator, makeFluentProvideDecorator} from "inversify-binding-decorators";

export const kernel = new Container();
export const provide = makeProvideDecorator(kernel);
const fluentProvide = makeFluentProvideDecorator(kernel);
export const service = function (id: interfaces.ServiceIdentifier<any>) {
    return fluentProvide(id)
        .inSingletonScope()
        .done();
};

export const {
    lazyInject,
    lazyInjectNamed,
    lazyInjectTagged,
    lazyMultiInject,
} = getDecorators(kernel);
