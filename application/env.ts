import {cpus} from "os";
const env = process.env;

export class Env {
    public static get develop(): boolean {
        return env.NODE_ENV != "production";
    }

    public static get production(): boolean {
        return env.NODE_ENV == "production";
    }

    public static get maxCPU(): number {
        return Math.min(env.MAX_CPU || 1, cpus().length);
    };

    /**
     * Whether investments are not limited by plan transactions count.
     * @returns {boolean}
     */
    public static get lifetimeInvestments(): boolean {
        return true;
    }
}
