"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var os_1 = require("os");
var env = process.env;
var Env = (function () {
    function Env() {
    }
    Object.defineProperty(Env, "develop", {
        get: function () {
            return env.NODE_ENV != "production";
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(Env, "production", {
        get: function () {
            return env.NODE_ENV == "production";
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(Env, "maxCPU", {
        get: function () {
            return Math.min(env.MAX_CPU || 1, os_1.cpus().length);
        },
        enumerable: true,
        configurable: true
    });
    ;
    Object.defineProperty(Env, "lifetimeInvestments", {
        get: function () {
            return true;
        },
        enumerable: true,
        configurable: true
    });
    return Env;
}());
exports.Env = Env;
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiL3Zhci93d3cvaHRtbC9hcHBsaWNhdGlvbi9lbnYudHMiLCJzb3VyY2VzIjpbIi92YXIvd3d3L2h0bWwvYXBwbGljYXRpb24vZW52LnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7O0FBQUEseUJBQXdCO0FBQ3hCLElBQU0sR0FBRyxHQUFHLE9BQU8sQ0FBQyxHQUFHLENBQUM7QUFFeEI7SUFBQTtJQW9CQSxDQUFDO0lBbkJHLHNCQUFrQixjQUFPO2FBQXpCO1lBQ0ksTUFBTSxDQUFDLEdBQUcsQ0FBQyxRQUFRLElBQUksWUFBWSxDQUFDO1FBQ3hDLENBQUM7OztPQUFBO0lBRUQsc0JBQWtCLGlCQUFVO2FBQTVCO1lBQ0ksTUFBTSxDQUFDLEdBQUcsQ0FBQyxRQUFRLElBQUksWUFBWSxDQUFDO1FBQ3hDLENBQUM7OztPQUFBO0lBRUQsc0JBQWtCLGFBQU07YUFBeEI7WUFDSSxNQUFNLENBQUMsSUFBSSxDQUFDLEdBQUcsQ0FBQyxHQUFHLENBQUMsT0FBTyxJQUFJLENBQUMsRUFBRSxTQUFJLEVBQUUsQ0FBQyxNQUFNLENBQUMsQ0FBQztRQUNyRCxDQUFDOzs7T0FBQTtJQUFBLENBQUM7SUFNRixzQkFBa0IsMEJBQW1CO2FBQXJDO1lBQ0ksTUFBTSxDQUFDLElBQUksQ0FBQztRQUNoQixDQUFDOzs7T0FBQTtJQUNMLFVBQUM7QUFBRCxDQUFDLEFBcEJELElBb0JDO0FBcEJZLGtCQUFHIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IHtjcHVzfSBmcm9tIFwib3NcIjtcbmNvbnN0IGVudiA9IHByb2Nlc3MuZW52O1xuXG5leHBvcnQgY2xhc3MgRW52IHtcbiAgICBwdWJsaWMgc3RhdGljIGdldCBkZXZlbG9wKCk6IGJvb2xlYW4ge1xuICAgICAgICByZXR1cm4gZW52Lk5PREVfRU5WICE9IFwicHJvZHVjdGlvblwiO1xuICAgIH1cblxuICAgIHB1YmxpYyBzdGF0aWMgZ2V0IHByb2R1Y3Rpb24oKTogYm9vbGVhbiB7XG4gICAgICAgIHJldHVybiBlbnYuTk9ERV9FTlYgPT0gXCJwcm9kdWN0aW9uXCI7XG4gICAgfVxuXG4gICAgcHVibGljIHN0YXRpYyBnZXQgbWF4Q1BVKCk6IG51bWJlciB7XG4gICAgICAgIHJldHVybiBNYXRoLm1pbihlbnYuTUFYX0NQVSB8fCAxLCBjcHVzKCkubGVuZ3RoKTtcbiAgICB9O1xuXG4gICAgLyoqXG4gICAgICogV2hldGhlciBpbnZlc3RtZW50cyBhcmUgbm90IGxpbWl0ZWQgYnkgcGxhbiB0cmFuc2FjdGlvbnMgY291bnQuXG4gICAgICogQHJldHVybnMge2Jvb2xlYW59XG4gICAgICovXG4gICAgcHVibGljIHN0YXRpYyBnZXQgbGlmZXRpbWVJbnZlc3RtZW50cygpOiBib29sZWFuIHtcbiAgICAgICAgcmV0dXJuIHRydWU7XG4gICAgfVxufVxuIl19