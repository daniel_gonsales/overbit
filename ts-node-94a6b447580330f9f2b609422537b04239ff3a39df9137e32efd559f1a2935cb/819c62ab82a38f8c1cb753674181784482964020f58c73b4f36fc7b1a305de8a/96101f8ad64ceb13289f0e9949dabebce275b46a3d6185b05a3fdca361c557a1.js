"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
var Express = require("express");
var Promise = require("bluebird");
var Utils = require("../../components/Utils");
var HttpStatus = require("http-status-codes");
var DefaultModule_1 = require("../DefaultModule");
var BlockTrail = require('blocktrail-sdk');
var MIN_WITHDRAW = BlockTrail.toSatoshi(0.0001);
var LoginModule = (function () {
    function LoginModule() {
        this.name = "login";
        this.url = "/user";
    }
    LoginModule.prototype.init = function () { };
    ;
    LoginModule.prototype.getRoute = function () {
        var router = Express.Router();
        router.post("/login", this.makeLogin.bind(this));
        router.post("/signup", this.makeSignup.bind(this));
        router.get("/logout", this.makeLogout.bind(this));
        router.get("/check", this.checkUser.bind(this));
        router.get("/stats", this.getUserStats.bind(this));
        router.post("/withdraw", this.makeWithdraw.bind(this));
        return router;
    };
    LoginModule.prototype.makeLogin = function (request, response) {
        var _this = this;
        this.verifyLoginRequest(request)
            .then(function (body) {
            return _this.userByUsername(body.username)
                .then(function (user) { return _this.verifyUserLogin(body, user); })
                .then(function (user) {
                request.session.login = user;
                Utils.respond.call(response, 200);
            })
                .catch(function (error) {
                console.log(error);
                Utils.respond.call(response, 500, "Internal Server Error");
            });
        })
            .catch(function (error) {
            Utils.respond.call(response, 400, error);
        });
    };
    LoginModule.prototype.makeSignup = function (request, response) {
        var _this = this;
        this.verifySignupRequest(request)
            .then(function (body) {
            _this.findWallet(body.wallet)
                .then(function (address) { return _this.createUser(body, address); })
                .then(function (user) {
                request.session.login = user;
                Utils.respond.call(response, 200);
            })
                .catch(function (error) {
                console.log(error);
                Utils.respond.call(response, 500, "Internal Server Error");
            });
        })
            .catch(function (error) {
            Utils.respond.call(response, 400, error);
        });
    };
    LoginModule.prototype.getUserStats = function (request, response) {
        var userID = request.session["login"].id;
        var promises = [];
        promises.push(this.queryUserBalanceSummary(userID), this.db.query("\n                SELECT `amount`, `transaction_hash`\n                FROM `transactions`\n                WHERE `transaction_type` = \"WITHDRAWAL\" AND `processed` = \"Y\"\n                AND `description` LIKE \"Wit%\" AND `user_id` = ?\n                LIMIT 10\n            ", [userID]), this.db.query("\n                SELECT `amount`, `transaction_hash`, `username`\n                FROM `transactions`\n                LEFT JOIN `login` ON `transactions`.`user_id` = `login`.`id`\n                WHERE `transaction_type` IN (\"WITHDRAWAL\",\"VIRTUAL\") AND `processed` = 'Y'\n                AND `description` LIKE \"Ref%\" AND `user_id` = ?\n                ORDER BY `process_time` DESC\n                LIMIT 10\n            ", [userID]));
        Promise.all(promises)
            .then(function (results) {
            var summary = results[0];
            var stats = {
                payouts: results[1][0],
                last_ref: results[2][0],
                deposits: summary.investments,
                withdrawals: summary.withdrawals,
                referrals: summary.earnings,
                balance: summary.balance,
                available_balance: summary.availableBalance,
                active_deposit: summary.principal,
            };
            Utils.respond.call(response, 200, stats);
        })
            .catch(function (error) {
            console.error(error);
            Utils.respond.call(response, 500);
        });
    };
    LoginModule.prototype.makeLogout = function (request, response) {
        request.session.login = null;
        Utils.respond.call(response, 200, "OK");
    };
    LoginModule.prototype.checkUser = function (request, response) {
        Utils.respond.call(response, 200, request.session["login"]);
    };
    LoginModule.prototype.makeWithdraw = function (request, response) {
        var _this = this;
        Promise.resolve().then(function () {
            var body = request.body;
            var amount = BlockTrail.toSatoshi(body.amount);
            if (isNaN(amount) || amount < 0)
                return Promise.reject("invalid input");
            if (amount < MIN_WITHDRAW)
                return Promise.reject("amount is less than lower bound");
            var userID = request.session["login"].id;
            return _this.queryUserBalanceSummary(userID)
                .then(function (summary) {
                if (amount > summary.availableBalance)
                    return Promise.reject("not enough funds");
                var fromEarnings = 0;
                var fromPrincipal = 0;
                switch (body.account_type) {
                    case "principal":
                        fromPrincipal = amount;
                        break;
                    case "earnings":
                        fromEarnings = amount;
                        break;
                    default:
                        if (amount <= summary.availableEarnings) {
                            fromEarnings = amount;
                        }
                        else {
                            fromEarnings = summary.availableEarnings;
                            fromPrincipal = amount - fromEarnings;
                        }
                        break;
                }
                if (fromPrincipal > summary.availablePrincipal)
                    return Promise.reject("not enough principal");
                if (fromEarnings > summary.availableEarnings)
                    return Promise.reject("not enough earnings");
                console.log("withdraw amount: " + amount + " where principal: " + fromPrincipal + " earnings: " + fromEarnings);
                _this.queryUserWalletWithPlan(userID)
                    .then(function (info) {
                    var hash;
                    var description;
                    var values = [];
                    if (fromEarnings > 0) {
                        var value = (-fromEarnings).toFixed(0);
                        hash = "VIRTUAL_" + info.user_wallet + "_" + userID + "_" + value;
                        description = "Withdrawal EARNINGS by user " + BlockTrail.toBTC(value) + " BTC to " + info.user_wallet + ")";
                        values.push("(now(), 'VIRTUAL', 'EARNINGS', '" + hash + "', 1, '" + userID + "', '" + value + "', 0, 'Y', '" + description + "')");
                    }
                    if (fromPrincipal > 0) {
                        var value = (-fromPrincipal).toFixed(0);
                        hash = "VIRTUAL_" + info.user_wallet + "_" + userID + "_" + value;
                        description = "Withdrawal PRINCIPAL by user " + BlockTrail.toBTC(value) + " BTC to " + info.user_wallet + ")";
                        values.push("(now(), 'VIRTUAL', 'PRINCIPAL', '" + hash + "', 1, '" + userID + "', '" + value + "', 0, 'Y', '" + description + "')");
                    }
                    {
                        var value = amount.toFixed(0);
                        hash = "WITHDRAWAL_" + info.user_wallet + "_" + userID + "_" + value;
                        description = "Withdrawal by user " + BlockTrail.toBTC(value) + " BTC to " + info.user_wallet + ")";
                        values.push("(now(), 'WITHDRAWAL', null, '" + hash + "', 1, '" + userID + "', '" + value + "', 0, 'N', '" + description + "')");
                    }
                    return _this.db.query("\n                                    INSERT INTO `transactions`\n                                    (`process_time`, `transaction_type`, `account_type`, `transaction_hash`, `payment_id`, `user_id`, `amount`, `confirmations`, `processed`, `description`)\n                                    VALUES\n                                    " + values.join(', ') + "\n                                ");
                })
                    .then(function () {
                    console.info("withdraw of '" + amount + "BTC' by user '" + userID + "' scheduled successfully");
                    _this.respond(response, HttpStatus.ACCEPTED);
                })
                    .catch(function (e) {
                    console.error("error while creating withdraw transaction", e);
                    _this.respond(response, HttpStatus.INTERNAL_SERVER_ERROR);
                });
            });
        }).catch(function (e) {
            console.log("withdraw reject", e);
            _this.respond(response, HttpStatus.BAD_REQUEST, e);
        });
    };
    LoginModule.prototype.verifyLoginRequest = function (request) {
        var body = request.body;
        if (!body.username)
            return Promise.reject("username is empty!");
        if (!body.password)
            return Promise.reject("password is empty!");
        return Promise.resolve(body);
    };
    LoginModule.prototype.verifySignupRequest = function (request) {
        var body = request.body;
        if (!body.username)
            return Promise.reject("username is empty!");
        if (!body.password)
            return Promise.reject("password is empty!");
        if (!body.email)
            return Promise.reject("email is empty!");
        if (!body.wallet)
            return Promise.reject("wallet is empty!");
        return this.userByUsername(body.username)
            .then(function (user) { return user ? Promise.reject("user with same username already exist!") : null; })
            .then(function () { return body; });
    };
    LoginModule.prototype.findWallet = function (wallet) {
        var _this = this;
        return new Promise(function (resolve, reject) {
            _this.blocktrail.address(wallet, function (error, address) {
                if (error)
                    reject(error);
                else if (!address)
                    reject("address is empty");
                else
                    resolve(address);
            });
        });
    };
    LoginModule.prototype.verifyUserLogin = function (body, user) {
        if (!user)
            return Promise.reject("user not exist");
        if (user.password != body.password)
            return Promise.reject("invalid password");
        return Promise.resolve(user);
    };
    LoginModule.prototype.userByUsername = function (username) {
        return this.db.query("SELECT * FROM `login` WHERE `username` = ?", [username])
            .then(function (rows) {
            var row = rows[0];
            if (row && row.length == 1)
                return row[0];
        });
    };
    LoginModule.prototype.createUser = function (body, address) {
        var referral = body.referral || null;
        return this.db.query("INSERT INTO `login` (`email`, `username`, `password`, `user_wallet`, `referral`)" +
            "VALUES (?, ?, ?, ? ,?)", [body.email, body.username, body.password, address.address, referral])
            .then(function (rows) {
            return {
                id: rows[0].insertId,
                user_wallet: address.address,
                points: 0,
                referral: referral,
                role: "USER"
            };
        });
    };
    LoginModule.prototype.respond = function (response, status, body) {
        Utils.respond.call(response, status, body);
    };
    LoginModule.prototype.queryUserInvestmentsTotal = function (userID, processedOnly) {
        if (processedOnly === void 0) { processedOnly = true; }
        var condition = processedOnly ? "AND `processed` = 'Y'" : '';
        var query = "\n        SELECT sum(`amount`) as sum\n        FROM `transactions`\n        LEFT JOIN `wallets` ON `transactions`.`wallet_id` = `wallets`.`id`\n        LEFT JOIN `login` ON `login`.`id` = `wallets`.`user_id`\n        WHERE `transaction_type` = \"DEPOSIT\"\n        AND `confirmations` > 0\n        " + condition + "\n        AND `login`.`id` = ?\n        ";
        return this.db.query(query, [userID])
            .then(function (results) { return Number(results[0][0]['sum']) || 0; });
    };
    LoginModule.prototype.queryUserFrozenInvestmentsTotal = function (userID, timeDelta) {
        if (timeDelta === void 0) { timeDelta = 24 * 60 * 60; }
        var query = "\n        SELECT sum(`amount`) as sum\n        FROM `transactions`\n        LEFT JOIN `wallets` ON `transactions`.`wallet_id` = `wallets`.`id`\n        LEFT JOIN `login` ON `login`.`id` = `wallets`.`user_id`\n        WHERE `transaction_type` = \"DEPOSIT\" AND `login`.`id` = ?\n        AND ((`transactions`.`processed` != 'Y') OR (`transactions`.`process_time` > (NOW() - " + timeDelta + ")))\n        ";
        return this.db.query(query, [userID])
            .then(function (results) { return Number(results[0][0]['sum']) || 0; });
    };
    LoginModule.prototype.queryUserEarningsTotal = function (userID) {
        var query = "\n        SELECT sum(`amount`) as sum\n        FROM `transactions`\n        WHERE ((`transaction_type` = \"WITHDRAWAL\" AND `description` LIKE \"Ref%\")\n        OR (`transaction_type` = \"VIRTUAL\"))\n        AND `processed` = \"Y\" AND `user_id` = ?\n        ";
        return this.db.query(query, [userID])
            .then(function (results) { return Number(results[0][0]['sum']) || 0; });
    };
    LoginModule.prototype.queryUserWithdrawalsTotal = function (userID) {
        return this.db.query("\n                SELECT sum(`amount`) as sum\n                FROM `transactions`\n                WHERE `transaction_type` = \"WITHDRAWAL\" AND `processed` = \"Y\" AND `description` LIKE \"Wit%\" AND `user_id` = ?\n            ", [userID])
            .then(function (results) { return Number(results[0][0]['sum']) || 0; });
    };
    LoginModule.prototype.queryUserBalanceSummary = function (userID) {
        var _this = this;
        return this.selectCompactTransactionsByUser(userID).then(function (transactions) {
            return _this.calculateUserAccountSummary(transactions);
        });
    };
    LoginModule.prototype.queryUserWalletWithPlan = function (userID) {
        return this.db.query("\n            SELECT `user_wallet`, `transactions`, `interval`\n            FROM `login`\n            LEFT JOIN `wallets` ON `wallets`.`user_id` = `login`.`id`\n            LEFT JOIN `plans` ON `plans`.`id` = `wallets`.`plan_id`\n            WHERE `user_id` = ?\n            ", [userID])
            .then(function (results) { return results[0][0]; });
    };
    LoginModule.prototype.calculateUserAccountSummary = function (transactions) {
        var hours24 = 24 * 60 * 60 * 1000;
        var unfreezeTime = new Date(Date.now() - hours24);
        var investments = 0;
        var withdrawals = 0;
        var earnings = 0;
        var frozen = 0;
        var earningsWithdrawals = 0;
        var principalWithdrawals = 0;
        var amount;
        var processTime;
        for (var _i = 0, transactions_1 = transactions; _i < transactions_1.length; _i++) {
            var transaction = transactions_1[_i];
            amount = transaction.amount;
            switch (transaction.transaction_type) {
                case 'DEPOSIT':
                    investments += amount;
                    processTime = new Date(transaction.process_time);
                    if (transaction.processed == 'N' ||
                        processTime > unfreezeTime)
                        frozen += amount;
                    break;
                case 'WITHDRAWAL':
                    withdrawals += amount;
                    break;
                case 'VIRTUAL':
                    if (transaction.processed != 'Y')
                        break;
                    switch (transaction.account_type) {
                        case 'PRINCIPAL':
                            if (amount < 0)
                                principalWithdrawals -= amount;
                            break;
                        default:
                            if (amount > 0)
                                earnings += amount;
                            if (amount < 0)
                                earningsWithdrawals -= amount;
                            break;
                    }
                    break;
            }
        }
        var balance = investments + earnings - withdrawals;
        var principal = investments - principalWithdrawals;
        return {
            investments: investments,
            earnings: earnings,
            withdrawals: withdrawals,
            principalWithdrawals: principalWithdrawals,
            earningsWithdrawals: earningsWithdrawals,
            balance: balance,
            frozenInvestments: frozen,
            availableBalance: balance - frozen,
            availableEarnings: earnings - earningsWithdrawals,
            availablePrincipal: principal - frozen,
            principal: principal,
        };
    };
    LoginModule.prototype.selectCompactTransactionsByUser = function (userID) {
        return this.db.query("\n            SELECT DISTINCT `process_time`, `transaction_type`, `account_type`, `amount`, `confirmations`, `processed`\n            FROM `transactions`\n            LEFT JOIN `wallets` ON `transactions`.`user_id` = `wallets`.`user_id` \n                                                    OR `transactions`.`wallet_id` = `wallets`.`id`\n            WHERE `wallets`.`user_id` = ?\n        ", [userID]).then(function (results) { return results[0]; });
    };
    __decorate([
        DefaultModule_1.Authorize(["USER", "ADMIN"]),
        __metadata("design:type", Function),
        __metadata("design:paramtypes", [Object, Object]),
        __metadata("design:returntype", void 0)
    ], LoginModule.prototype, "getUserStats", null);
    __decorate([
        DefaultModule_1.Authorize(["USER", "ADMIN"]),
        __metadata("design:type", Function),
        __metadata("design:paramtypes", [Object, Object]),
        __metadata("design:returntype", void 0)
    ], LoginModule.prototype, "makeLogout", null);
    __decorate([
        DefaultModule_1.Authorize(["USER", "ADMIN"]),
        __metadata("design:type", Function),
        __metadata("design:paramtypes", [Object, Object]),
        __metadata("design:returntype", void 0)
    ], LoginModule.prototype, "checkUser", null);
    __decorate([
        DefaultModule_1.Authorize(["USER", "ADMIN"]),
        __metadata("design:type", Function),
        __metadata("design:paramtypes", [Object, Object]),
        __metadata("design:returntype", void 0)
    ], LoginModule.prototype, "makeWithdraw", null);
    LoginModule = __decorate([
        DefaultModule_1.Register
    ], LoginModule);
    return LoginModule;
}());
exports.LoginModule = LoginModule;
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiL3Zhci93d3cvaHRtbC9hcHBsaWNhdGlvbi9tb2R1bGVzL2xvZ2luL0xvZ2luTW9kdWxlLnRzIiwic291cmNlcyI6WyIvdmFyL3d3dy9odG1sL2FwcGxpY2F0aW9uL21vZHVsZXMvbG9naW4vTG9naW5Nb2R1bGUudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7Ozs7Ozs7Ozs7QUFBQSxpQ0FBbUM7QUFDbkMsa0NBQW9DO0FBQ3BDLDhDQUFnRDtBQUNoRCw4Q0FBZ0Q7QUFDaEQsa0RBQThEO0FBQzlELElBQU0sVUFBVSxHQUFRLE9BQU8sQ0FBQyxnQkFBZ0IsQ0FBQyxDQUFDO0FBQ2xELElBQU0sWUFBWSxHQUFHLFVBQVUsQ0FBQyxTQUFTLENBQUMsTUFBTSxDQUFDLENBQUM7QUFHbEQ7SUFEQTtRQUVXLFNBQUksR0FBVyxPQUFPLENBQUM7UUFDdkIsUUFBRyxHQUFXLE9BQU8sQ0FBQztJQXViakMsQ0FBQztJQWxiVSwwQkFBSSxHQUFYLGNBQWUsQ0FBQztJQUFBLENBQUM7SUFFViw4QkFBUSxHQUFmO1FBQ0ksSUFBSSxNQUFNLEdBQUcsT0FBTyxDQUFDLE1BQU0sRUFBRSxDQUFDO1FBRTlCLE1BQU0sQ0FBQyxJQUFJLENBQUMsUUFBUSxFQUFFLElBQUksQ0FBQyxTQUFTLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxDQUFDLENBQUM7UUFDakQsTUFBTSxDQUFDLElBQUksQ0FBQyxTQUFTLEVBQUUsSUFBSSxDQUFDLFVBQVUsQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLENBQUMsQ0FBQztRQUNuRCxNQUFNLENBQUMsR0FBRyxDQUFDLFNBQVMsRUFBRSxJQUFJLENBQUMsVUFBVSxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsQ0FBQyxDQUFDO1FBQ2xELE1BQU0sQ0FBQyxHQUFHLENBQUMsUUFBUSxFQUFFLElBQUksQ0FBQyxTQUFTLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxDQUFDLENBQUM7UUFDaEQsTUFBTSxDQUFDLEdBQUcsQ0FBQyxRQUFRLEVBQUUsSUFBSSxDQUFDLFlBQVksQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLENBQUMsQ0FBQztRQUNuRCxNQUFNLENBQUMsSUFBSSxDQUFDLFdBQVcsRUFBRSxJQUFJLENBQUMsWUFBWSxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsQ0FBQyxDQUFDO1FBRXZELE1BQU0sQ0FBQyxNQUFNLENBQUM7SUFDbEIsQ0FBQztJQUVPLCtCQUFTLEdBQWpCLFVBQWtCLE9BQVksRUFBRSxRQUFhO1FBQTdDLGlCQWtCQztRQWhCRyxJQUFJLENBQUMsa0JBQWtCLENBQUMsT0FBTyxDQUFDO2FBQzNCLElBQUksQ0FBQyxVQUFBLElBQUk7WUFDTixNQUFNLENBQUMsS0FBSSxDQUFDLGNBQWMsQ0FBQyxJQUFJLENBQUMsUUFBUSxDQUFDO2lCQUNwQyxJQUFJLENBQUMsVUFBQSxJQUFJLElBQUksT0FBQSxLQUFJLENBQUMsZUFBZSxDQUFDLElBQUksRUFBRSxJQUFJLENBQUMsRUFBaEMsQ0FBZ0MsQ0FBQztpQkFDOUMsSUFBSSxDQUFDLFVBQUEsSUFBSTtnQkFDTixPQUFPLENBQUMsT0FBTyxDQUFDLEtBQUssR0FBRyxJQUFJLENBQUM7Z0JBQzdCLEtBQUssQ0FBQyxPQUFPLENBQUMsSUFBSSxDQUFDLFFBQVEsRUFBRSxHQUFHLENBQUMsQ0FBQztZQUN0QyxDQUFDLENBQUM7aUJBQ0QsS0FBSyxDQUFDLFVBQUMsS0FBVTtnQkFDZCxPQUFPLENBQUMsR0FBRyxDQUFDLEtBQUssQ0FBQyxDQUFDO2dCQUNuQixLQUFLLENBQUMsT0FBTyxDQUFDLElBQUksQ0FBQyxRQUFRLEVBQUUsR0FBRyxFQUFFLHVCQUF1QixDQUFDLENBQUM7WUFDL0QsQ0FBQyxDQUFDLENBQUM7UUFDWCxDQUFDLENBQUM7YUFDRCxLQUFLLENBQUMsVUFBQSxLQUFLO1lBQ1IsS0FBSyxDQUFDLE9BQU8sQ0FBQyxJQUFJLENBQUMsUUFBUSxFQUFFLEdBQUcsRUFBRSxLQUFLLENBQUMsQ0FBQztRQUM3QyxDQUFDLENBQUMsQ0FBQztJQUNYLENBQUM7SUFFTyxnQ0FBVSxHQUFsQixVQUFtQixPQUFZLEVBQUUsUUFBYTtRQUE5QyxpQkFpQkM7UUFoQkcsSUFBSSxDQUFDLG1CQUFtQixDQUFDLE9BQU8sQ0FBQzthQUM1QixJQUFJLENBQUMsVUFBQSxJQUFJO1lBQ04sS0FBSSxDQUFDLFVBQVUsQ0FBQyxJQUFJLENBQUMsTUFBTSxDQUFDO2lCQUN2QixJQUFJLENBQUMsVUFBQSxPQUFPLElBQUksT0FBQSxLQUFJLENBQUMsVUFBVSxDQUFDLElBQUksRUFBRSxPQUFPLENBQUMsRUFBOUIsQ0FBOEIsQ0FBQztpQkFDL0MsSUFBSSxDQUFDLFVBQUEsSUFBSTtnQkFDTixPQUFPLENBQUMsT0FBTyxDQUFDLEtBQUssR0FBRyxJQUFJLENBQUM7Z0JBQzdCLEtBQUssQ0FBQyxPQUFPLENBQUMsSUFBSSxDQUFDLFFBQVEsRUFBRSxHQUFHLENBQUMsQ0FBQztZQUN0QyxDQUFDLENBQUM7aUJBQ0QsS0FBSyxDQUFDLFVBQUMsS0FBVTtnQkFDZCxPQUFPLENBQUMsR0FBRyxDQUFDLEtBQUssQ0FBQyxDQUFDO2dCQUNuQixLQUFLLENBQUMsT0FBTyxDQUFDLElBQUksQ0FBQyxRQUFRLEVBQUUsR0FBRyxFQUFFLHVCQUF1QixDQUFDLENBQUM7WUFDL0QsQ0FBQyxDQUFDLENBQUM7UUFDWCxDQUFDLENBQUM7YUFDRCxLQUFLLENBQUMsVUFBQSxLQUFLO1lBQ1IsS0FBSyxDQUFDLE9BQU8sQ0FBQyxJQUFJLENBQUMsUUFBUSxFQUFFLEdBQUcsRUFBRSxLQUFLLENBQUMsQ0FBQztRQUM3QyxDQUFDLENBQUMsQ0FBQztJQUNYLENBQUM7SUFHTyxrQ0FBWSxHQUFwQixVQUFxQixPQUFZLEVBQUUsUUFBYTtRQUM1QyxJQUFNLE1BQU0sR0FBRyxPQUFPLENBQUMsT0FBTyxDQUFDLE9BQU8sQ0FBQyxDQUFDLEVBQUUsQ0FBQztRQUMzQyxJQUFJLFFBQVEsR0FBRyxFQUFFLENBQUM7UUFFbEIsUUFBUSxDQUFDLElBQUksQ0FDVCxJQUFJLENBQUMsdUJBQXVCLENBQUMsTUFBTSxDQUFDLEVBRXBDLElBQUksQ0FBQyxFQUFFLENBQUMsS0FBSyxDQUFDLDBSQU1iLEVBQUUsQ0FBQyxNQUFNLENBQUMsQ0FBQyxFQUVaLElBQUksQ0FBQyxFQUFFLENBQUMsS0FBSyxDQUFDLCthQVFiLEVBQUUsQ0FBQyxNQUFNLENBQUMsQ0FBQyxDQUNmLENBQUM7UUFFRixPQUFPLENBQUMsR0FBRyxDQUFDLFFBQVEsQ0FBQzthQUNoQixJQUFJLENBQUMsVUFBQyxPQUFjO1lBQ2pCLElBQU0sT0FBTyxHQUFHLE9BQU8sQ0FBQyxDQUFDLENBQUMsQ0FBQztZQUMzQixJQUFJLEtBQUssR0FBRztnQkFDUixPQUFPLEVBQUUsT0FBTyxDQUFDLENBQUMsQ0FBQyxDQUFDLENBQUMsQ0FBQztnQkFDdEIsUUFBUSxFQUFFLE9BQU8sQ0FBQyxDQUFDLENBQUMsQ0FBQyxDQUFDLENBQUM7Z0JBQ3ZCLFFBQVEsRUFBRSxPQUFPLENBQUMsV0FBVztnQkFDN0IsV0FBVyxFQUFFLE9BQU8sQ0FBQyxXQUFXO2dCQUNoQyxTQUFTLEVBQUUsT0FBTyxDQUFDLFFBQVE7Z0JBQzNCLE9BQU8sRUFBRSxPQUFPLENBQUMsT0FBTztnQkFDeEIsaUJBQWlCLEVBQUUsT0FBTyxDQUFDLGdCQUFnQjtnQkFDM0MsY0FBYyxFQUFFLE9BQU8sQ0FBQyxTQUFTO2FBQ3BDLENBQUM7WUFDRixLQUFLLENBQUMsT0FBTyxDQUFDLElBQUksQ0FBQyxRQUFRLEVBQUUsR0FBRyxFQUFFLEtBQUssQ0FBQyxDQUFDO1FBQzdDLENBQUMsQ0FBQzthQUNELEtBQUssQ0FBQyxVQUFDLEtBQUs7WUFDVCxPQUFPLENBQUMsS0FBSyxDQUFDLEtBQUssQ0FBQyxDQUFDO1lBQ3JCLEtBQUssQ0FBQyxPQUFPLENBQUMsSUFBSSxDQUFDLFFBQVEsRUFBRSxHQUFHLENBQUMsQ0FBQztRQUN0QyxDQUFDLENBQUMsQ0FBQztJQUNYLENBQUM7SUFHTyxnQ0FBVSxHQUFsQixVQUFtQixPQUFZLEVBQUUsUUFBYTtRQUMxQyxPQUFPLENBQUMsT0FBTyxDQUFDLEtBQUssR0FBRyxJQUFJLENBQUM7UUFDN0IsS0FBSyxDQUFDLE9BQU8sQ0FBQyxJQUFJLENBQUMsUUFBUSxFQUFFLEdBQUcsRUFBRSxJQUFJLENBQUMsQ0FBQztJQUM1QyxDQUFDO0lBR08sK0JBQVMsR0FBakIsVUFBa0IsT0FBWSxFQUFFLFFBQWE7UUFDekMsS0FBSyxDQUFDLE9BQU8sQ0FBQyxJQUFJLENBQUMsUUFBUSxFQUFFLEdBQUcsRUFBRSxPQUFPLENBQUMsT0FBTyxDQUFDLE9BQU8sQ0FBQyxDQUFDLENBQUM7SUFDaEUsQ0FBQztJQUdPLGtDQUFZLEdBQXBCLFVBQXFCLE9BQVksRUFBRSxRQUFhO1FBRGhELGlCQXFHQztRQW5HRyxPQUFPLENBQUMsT0FBTyxFQUFFLENBQUMsSUFBSSxDQUFDO1lBRW5CLElBQU0sSUFBSSxHQUFHLE9BQU8sQ0FBQyxJQUFxQixDQUFDO1lBQzNDLElBQU0sTUFBTSxHQUFHLFVBQVUsQ0FBQyxTQUFTLENBQUMsSUFBSSxDQUFDLE1BQU0sQ0FBQyxDQUFDO1lBR2pELEVBQUUsQ0FBQyxDQUFDLEtBQUssQ0FBQyxNQUFNLENBQUMsSUFBSSxNQUFNLEdBQUcsQ0FBQyxDQUFDO2dCQUM1QixNQUFNLENBQUMsT0FBTyxDQUFDLE1BQU0sQ0FBQyxlQUFlLENBQUMsQ0FBQztZQUczQyxFQUFFLENBQUMsQ0FBQyxNQUFNLEdBQUcsWUFBWSxDQUFDO2dCQUN0QixNQUFNLENBQUMsT0FBTyxDQUFDLE1BQU0sQ0FBQyxpQ0FBaUMsQ0FBQyxDQUFDO1lBRTdELElBQU0sTUFBTSxHQUFHLE9BQU8sQ0FBQyxPQUFPLENBQUMsT0FBTyxDQUFDLENBQUMsRUFBRSxDQUFDO1lBQzNDLE1BQU0sQ0FBQyxLQUFJLENBQUMsdUJBQXVCLENBQUMsTUFBTSxDQUFDO2lCQUN0QyxJQUFJLENBQUMsVUFBQSxPQUFPO2dCQUVULEVBQUUsQ0FBQyxDQUFDLE1BQU0sR0FBRyxPQUFPLENBQUMsZ0JBQWdCLENBQUM7b0JBQ2xDLE1BQU0sQ0FBQyxPQUFPLENBQUMsTUFBTSxDQUFDLGtCQUFrQixDQUFDLENBQUM7Z0JBRzlDLElBQUksWUFBWSxHQUFHLENBQUMsQ0FBQztnQkFDckIsSUFBSSxhQUFhLEdBQUcsQ0FBQyxDQUFDO2dCQUN0QixNQUFNLENBQUMsQ0FBQyxJQUFJLENBQUMsWUFBWSxDQUFDLENBQUMsQ0FBQztvQkFDeEIsS0FBSyxXQUFXO3dCQUNaLGFBQWEsR0FBRyxNQUFNLENBQUM7d0JBQ3ZCLEtBQUssQ0FBQztvQkFDVixLQUFLLFVBQVU7d0JBQ1gsWUFBWSxHQUFHLE1BQU0sQ0FBQzt3QkFDdEIsS0FBSyxDQUFDO29CQUNWO3dCQUNJLEVBQUUsQ0FBQyxDQUFDLE1BQU0sSUFBSSxPQUFPLENBQUMsaUJBQWlCLENBQUMsQ0FBQyxDQUFDOzRCQUN0QyxZQUFZLEdBQUcsTUFBTSxDQUFDO3dCQUMxQixDQUFDO3dCQUNELElBQUksQ0FBQyxDQUFDOzRCQUNGLFlBQVksR0FBRyxPQUFPLENBQUMsaUJBQWlCLENBQUM7NEJBQ3pDLGFBQWEsR0FBRyxNQUFNLEdBQUcsWUFBWSxDQUFDO3dCQUMxQyxDQUFDO3dCQUNELEtBQUssQ0FBQTtnQkFDYixDQUFDO2dCQUdELEVBQUUsQ0FBQyxDQUFDLGFBQWEsR0FBRyxPQUFPLENBQUMsa0JBQWtCLENBQUM7b0JBQzNDLE1BQU0sQ0FBQyxPQUFPLENBQUMsTUFBTSxDQUFDLHNCQUFzQixDQUFDLENBQUM7Z0JBQ2xELEVBQUUsQ0FBQyxDQUFDLFlBQVksR0FBRyxPQUFPLENBQUMsaUJBQWlCLENBQUM7b0JBQ3pDLE1BQU0sQ0FBQyxPQUFPLENBQUMsTUFBTSxDQUFDLHFCQUFxQixDQUFDLENBQUM7Z0JBR2pELE9BQU8sQ0FBQyxHQUFHLENBQUMsc0JBQW9CLE1BQU0sMEJBQXFCLGFBQWEsbUJBQWMsWUFBYyxDQUFDLENBQUM7Z0JBQ3RHLEtBQUksQ0FBQyx1QkFBdUIsQ0FBQyxNQUFNLENBQUM7cUJBQy9CLElBQUksQ0FBQyxVQUFBLElBQUk7b0JBQ04sSUFBSSxJQUFZLENBQUM7b0JBQ2pCLElBQUksV0FBbUIsQ0FBQztvQkFDeEIsSUFBTSxNQUFNLEdBQUcsRUFBRSxDQUFDO29CQUdsQixFQUFFLENBQUMsQ0FBQyxZQUFZLEdBQUcsQ0FBQyxDQUFDLENBQUMsQ0FBQzt3QkFDbkIsSUFBSSxLQUFLLEdBQUcsQ0FBQyxDQUFDLFlBQVksQ0FBQyxDQUFDLE9BQU8sQ0FBQyxDQUFDLENBQUMsQ0FBQzt3QkFDdkMsSUFBSSxHQUFHLGFBQVcsSUFBSSxDQUFDLFdBQVcsU0FBSSxNQUFNLFNBQUksS0FBTyxDQUFDO3dCQUN4RCxXQUFXLEdBQUcsaUNBQStCLFVBQVUsQ0FBQyxLQUFLLENBQUMsS0FBSyxDQUFDLGdCQUFXLElBQUksQ0FBQyxXQUFXLE1BQUcsQ0FBQzt3QkFDbkcsTUFBTSxDQUFDLElBQUksQ0FBQyxxQ0FBbUMsSUFBSSxlQUFVLE1BQU0sWUFBTyxLQUFLLG9CQUFlLFdBQVcsT0FBSSxDQUFDLENBQUM7b0JBQ25ILENBQUM7b0JBR0QsRUFBRSxDQUFDLENBQUMsYUFBYSxHQUFHLENBQUMsQ0FBQyxDQUFDLENBQUM7d0JBQ3BCLElBQUksS0FBSyxHQUFHLENBQUMsQ0FBQyxhQUFhLENBQUMsQ0FBQyxPQUFPLENBQUMsQ0FBQyxDQUFDLENBQUM7d0JBQ3hDLElBQUksR0FBRyxhQUFXLElBQUksQ0FBQyxXQUFXLFNBQUksTUFBTSxTQUFJLEtBQU8sQ0FBQzt3QkFDeEQsV0FBVyxHQUFHLGtDQUFnQyxVQUFVLENBQUMsS0FBSyxDQUFDLEtBQUssQ0FBQyxnQkFBVyxJQUFJLENBQUMsV0FBVyxNQUFHLENBQUM7d0JBQ3BHLE1BQU0sQ0FBQyxJQUFJLENBQUMsc0NBQW9DLElBQUksZUFBVSxNQUFNLFlBQU8sS0FBSyxvQkFBZSxXQUFXLE9BQUksQ0FBQyxDQUFDO29CQUNwSCxDQUFDO29CQUdELENBQUM7d0JBQ0csSUFBSSxLQUFLLEdBQUcsTUFBTSxDQUFDLE9BQU8sQ0FBQyxDQUFDLENBQUMsQ0FBQzt3QkFDOUIsSUFBSSxHQUFHLGdCQUFjLElBQUksQ0FBQyxXQUFXLFNBQUksTUFBTSxTQUFJLEtBQU8sQ0FBQzt3QkFDM0QsV0FBVyxHQUFHLHdCQUFzQixVQUFVLENBQUMsS0FBSyxDQUFDLEtBQUssQ0FBQyxnQkFBVyxJQUFJLENBQUMsV0FBVyxNQUFHLENBQUM7d0JBQzFGLE1BQU0sQ0FBQyxJQUFJLENBQUMsa0NBQWdDLElBQUksZUFBVSxNQUFNLFlBQU8sS0FBSyxvQkFBZSxXQUFXLE9BQUksQ0FBQyxDQUFDO29CQUNoSCxDQUFDO29CQUVELE1BQU0sQ0FBQyxLQUFJLENBQUMsRUFBRSxDQUFDLEtBQUssQ0FBQyxxVkFJWCxNQUFNLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyx1Q0FDdEIsQ0FBQyxDQUFDO2dCQUNYLENBQUMsQ0FBQztxQkFDRCxJQUFJLENBQUM7b0JBQ0YsT0FBTyxDQUFDLElBQUksQ0FBQyxrQkFBZ0IsTUFBTSxzQkFBaUIsTUFBTSw2QkFBMEIsQ0FBQyxDQUFDO29CQUN0RixLQUFJLENBQUMsT0FBTyxDQUFDLFFBQVEsRUFBRSxVQUFVLENBQUMsUUFBUSxDQUFDLENBQUE7Z0JBQy9DLENBQUMsQ0FBQztxQkFDRCxLQUFLLENBQUMsVUFBQSxDQUFDO29CQUNKLE9BQU8sQ0FBQyxLQUFLLENBQUMsMkNBQTJDLEVBQUUsQ0FBQyxDQUFDLENBQUM7b0JBQzlELEtBQUksQ0FBQyxPQUFPLENBQUMsUUFBUSxFQUFFLFVBQVUsQ0FBQyxxQkFBcUIsQ0FBQyxDQUFDO2dCQUM3RCxDQUFDLENBQUMsQ0FBQztZQUNYLENBQUMsQ0FBQyxDQUFDO1FBQ1gsQ0FBQyxDQUFDLENBQUMsS0FBSyxDQUFDLFVBQUEsQ0FBQztZQUNOLE9BQU8sQ0FBQyxHQUFHLENBQUMsaUJBQWlCLEVBQUUsQ0FBQyxDQUFDLENBQUM7WUFDbEMsS0FBSSxDQUFDLE9BQU8sQ0FBQyxRQUFRLEVBQUUsVUFBVSxDQUFDLFdBQVcsRUFBRSxDQUFDLENBQUMsQ0FBQztRQUN0RCxDQUFDLENBQUMsQ0FBQTtJQUNOLENBQUM7SUFFTyx3Q0FBa0IsR0FBMUIsVUFBMkIsT0FBTztRQUM5QixJQUFNLElBQUksR0FBRyxPQUFPLENBQUMsSUFBa0IsQ0FBQztRQUN4QyxFQUFFLENBQUMsQ0FBQyxDQUFDLElBQUksQ0FBQyxRQUFRLENBQUM7WUFBQyxNQUFNLENBQUMsT0FBTyxDQUFDLE1BQU0sQ0FBQyxvQkFBb0IsQ0FBQyxDQUFDO1FBQ2hFLEVBQUUsQ0FBQyxDQUFDLENBQUMsSUFBSSxDQUFDLFFBQVEsQ0FBQztZQUFDLE1BQU0sQ0FBQyxPQUFPLENBQUMsTUFBTSxDQUFDLG9CQUFvQixDQUFDLENBQUM7UUFDaEUsTUFBTSxDQUFDLE9BQU8sQ0FBQyxPQUFPLENBQUMsSUFBSSxDQUFDLENBQUM7SUFDakMsQ0FBQztJQUVPLHlDQUFtQixHQUEzQixVQUE0QixPQUFPO1FBQy9CLElBQU0sSUFBSSxHQUFHLE9BQU8sQ0FBQyxJQUFtQixDQUFDO1FBQ3pDLEVBQUUsQ0FBQyxDQUFDLENBQUMsSUFBSSxDQUFDLFFBQVEsQ0FBQztZQUFDLE1BQU0sQ0FBQyxPQUFPLENBQUMsTUFBTSxDQUFDLG9CQUFvQixDQUFDLENBQUM7UUFDaEUsRUFBRSxDQUFDLENBQUMsQ0FBQyxJQUFJLENBQUMsUUFBUSxDQUFDO1lBQUMsTUFBTSxDQUFDLE9BQU8sQ0FBQyxNQUFNLENBQUMsb0JBQW9CLENBQUMsQ0FBQztRQUNoRSxFQUFFLENBQUMsQ0FBQyxDQUFDLElBQUksQ0FBQyxLQUFLLENBQUM7WUFBQyxNQUFNLENBQUMsT0FBTyxDQUFDLE1BQU0sQ0FBQyxpQkFBaUIsQ0FBQyxDQUFDO1FBQzFELEVBQUUsQ0FBQyxDQUFDLENBQUMsSUFBSSxDQUFDLE1BQU0sQ0FBQztZQUFDLE1BQU0sQ0FBQyxPQUFPLENBQUMsTUFBTSxDQUFDLGtCQUFrQixDQUFDLENBQUM7UUFDNUQsTUFBTSxDQUFDLElBQUksQ0FBQyxjQUFjLENBQUMsSUFBSSxDQUFDLFFBQVEsQ0FBQzthQUNwQyxJQUFJLENBQUMsVUFBQSxJQUFJLElBQUksT0FBQSxJQUFJLEdBQUcsT0FBTyxDQUFDLE1BQU0sQ0FBQyx3Q0FBd0MsQ0FBQyxHQUFHLElBQUksRUFBdEUsQ0FBc0UsQ0FBQzthQUNwRixJQUFJLENBQUMsY0FBTSxPQUFBLElBQUksRUFBSixDQUFJLENBQUMsQ0FBQztJQUMxQixDQUFDO0lBRU8sZ0NBQVUsR0FBbEIsVUFBbUIsTUFBTTtRQUF6QixpQkFRQztRQVBHLE1BQU0sQ0FBQyxJQUFJLE9BQU8sQ0FBQyxVQUFDLE9BQU8sRUFBRSxNQUFNO1lBQy9CLEtBQUksQ0FBQyxVQUFVLENBQUMsT0FBTyxDQUFDLE1BQU0sRUFBRSxVQUFDLEtBQUssRUFBRSxPQUFPO2dCQUMzQyxFQUFFLENBQUMsQ0FBQyxLQUFLLENBQUM7b0JBQUMsTUFBTSxDQUFDLEtBQUssQ0FBQyxDQUFDO2dCQUN6QixJQUFJLENBQUMsRUFBRSxDQUFDLENBQUMsQ0FBQyxPQUFPLENBQUM7b0JBQUMsTUFBTSxDQUFDLGtCQUFrQixDQUFDLENBQUM7Z0JBQzlDLElBQUk7b0JBQUMsT0FBTyxDQUFDLE9BQU8sQ0FBQyxDQUFDO1lBQzFCLENBQUMsQ0FBQyxDQUFDO1FBQ1AsQ0FBQyxDQUFDLENBQUM7SUFDUCxDQUFDO0lBRU8scUNBQWUsR0FBdkIsVUFBd0IsSUFBZ0IsRUFBRSxJQUFXO1FBQ2pELEVBQUUsQ0FBQyxDQUFDLENBQUMsSUFBSSxDQUFDO1lBQ04sTUFBTSxDQUFDLE9BQU8sQ0FBQyxNQUFNLENBQUMsZ0JBQWdCLENBQUMsQ0FBQztRQUc1QyxFQUFFLENBQUMsQ0FBQyxJQUFJLENBQUMsUUFBUSxJQUFJLElBQUksQ0FBQyxRQUFRLENBQUM7WUFDL0IsTUFBTSxDQUFDLE9BQU8sQ0FBQyxNQUFNLENBQUMsa0JBQWtCLENBQUMsQ0FBQztRQUU5QyxNQUFNLENBQUMsT0FBTyxDQUFDLE9BQU8sQ0FBQyxJQUFJLENBQUMsQ0FBQztJQUNqQyxDQUFDO0lBRU8sb0NBQWMsR0FBdEIsVUFBdUIsUUFBZ0I7UUFDbkMsTUFBTSxDQUFDLElBQUksQ0FBQyxFQUFFLENBQUMsS0FBSyxDQUFDLDRDQUE0QyxFQUFFLENBQUMsUUFBUSxDQUFDLENBQUM7YUFDekUsSUFBSSxDQUFDLFVBQUEsSUFBSTtZQUNOLElBQU0sR0FBRyxHQUFVLElBQUksQ0FBQyxDQUFDLENBQUMsQ0FBQztZQUMzQixFQUFFLENBQUMsQ0FBQyxHQUFHLElBQUksR0FBRyxDQUFDLE1BQU0sSUFBSSxDQUFDLENBQUM7Z0JBQ3ZCLE1BQU0sQ0FBQyxHQUFHLENBQUMsQ0FBQyxDQUFDLENBQUM7UUFDdEIsQ0FBQyxDQUFDLENBQUE7SUFDVixDQUFDO0lBRU8sZ0NBQVUsR0FBbEIsVUFBbUIsSUFBaUIsRUFBRSxPQUFZO1FBQzlDLElBQU0sUUFBUSxHQUFHLElBQUksQ0FBQyxRQUFRLElBQUksSUFBSSxDQUFDO1FBQ3ZDLE1BQU0sQ0FBQyxJQUFJLENBQUMsRUFBRSxDQUFDLEtBQUssQ0FBQyxrRkFBa0Y7WUFDbEYsd0JBQXdCLEVBQ3pDLENBQUMsSUFBSSxDQUFDLEtBQUssRUFBRSxJQUFJLENBQUMsUUFBUSxFQUFFLElBQUksQ0FBQyxRQUFRLEVBQUUsT0FBTyxDQUFDLE9BQU8sRUFBRSxRQUFRLENBQUMsQ0FBQzthQUNyRSxJQUFJLENBQUMsVUFBQSxJQUFJO1lBQ04sTUFBTSxDQUFDO2dCQUNILEVBQUUsRUFBRSxJQUFJLENBQUMsQ0FBQyxDQUFDLENBQUMsUUFBUTtnQkFDcEIsV0FBVyxFQUFFLE9BQU8sQ0FBQyxPQUFPO2dCQUM1QixNQUFNLEVBQUUsQ0FBQztnQkFDVCxRQUFRLEVBQUUsUUFBUTtnQkFDbEIsSUFBSSxFQUFFLE1BQU07YUFDZixDQUFBO1FBQ0wsQ0FBQyxDQUFDLENBQUM7SUFDWCxDQUFDO0lBRU8sNkJBQU8sR0FBZixVQUFnQixRQUFRLEVBQUUsTUFBYyxFQUFFLElBQVU7UUFDaEQsS0FBSyxDQUFDLE9BQU8sQ0FBQyxJQUFJLENBQUMsUUFBUSxFQUFFLE1BQU0sRUFBRSxJQUFJLENBQUMsQ0FBQztJQUMvQyxDQUFDO0lBRU8sK0NBQXlCLEdBQWpDLFVBQWtDLE1BQXVCLEVBQUUsYUFBNkI7UUFBN0IsOEJBQUEsRUFBQSxvQkFBNkI7UUFDcEYsSUFBTSxTQUFTLEdBQUcsYUFBYSxHQUFHLHVCQUF5QixHQUFHLEVBQUUsQ0FBQztRQUNqRSxJQUFNLEtBQUssR0FBRywrU0FPWixTQUFTLDZDQUVWLENBQUM7UUFDRixNQUFNLENBQUMsSUFBSSxDQUFDLEVBQUUsQ0FBQyxLQUFLLENBQUMsS0FBSyxFQUFFLENBQUMsTUFBTSxDQUFDLENBQUM7YUFDaEMsSUFBSSxDQUFDLFVBQUMsT0FBYyxJQUFLLE9BQUEsTUFBTSxDQUFDLE9BQU8sQ0FBQyxDQUFDLENBQUMsQ0FBQyxDQUFDLENBQUMsQ0FBQyxLQUFLLENBQUMsQ0FBQyxJQUFJLENBQUMsRUFBakMsQ0FBaUMsQ0FBQyxDQUFDO0lBQ3JFLENBQUM7SUFPTyxxREFBK0IsR0FBdkMsVUFBd0MsTUFBdUIsRUFBRSxTQUFnQztRQUFoQywwQkFBQSxFQUFBLFlBQW9CLEVBQUUsR0FBRyxFQUFFLEdBQUcsRUFBRTtRQUM3RixJQUFNLEtBQUssR0FBRyx5WEFNa0YsU0FBUyxrQkFDeEcsQ0FBQztRQUNGLE1BQU0sQ0FBQyxJQUFJLENBQUMsRUFBRSxDQUFDLEtBQUssQ0FBQyxLQUFLLEVBQUUsQ0FBQyxNQUFNLENBQUMsQ0FBQzthQUNoQyxJQUFJLENBQUMsVUFBQyxPQUFjLElBQUssT0FBQSxNQUFNLENBQUMsT0FBTyxDQUFDLENBQUMsQ0FBQyxDQUFDLENBQUMsQ0FBQyxDQUFDLEtBQUssQ0FBQyxDQUFDLElBQUksQ0FBQyxFQUFqQyxDQUFpQyxDQUFDLENBQUM7SUFDckUsQ0FBQztJQUVPLDRDQUFzQixHQUE5QixVQUErQixNQUF1QjtRQUVsRCxJQUFJLEtBQUssR0FBRyx1UUFNWCxDQUFDO1FBQ0YsTUFBTSxDQUFDLElBQUksQ0FBQyxFQUFFLENBQUMsS0FBSyxDQUFDLEtBQUssRUFBRSxDQUFDLE1BQU0sQ0FBQyxDQUFDO2FBQ2hDLElBQUksQ0FBQyxVQUFDLE9BQWMsSUFBSyxPQUFBLE1BQU0sQ0FBQyxPQUFPLENBQUMsQ0FBQyxDQUFDLENBQUMsQ0FBQyxDQUFDLENBQUMsS0FBSyxDQUFDLENBQUMsSUFBSSxDQUFDLEVBQWpDLENBQWlDLENBQUMsQ0FBQztJQUNyRSxDQUFDO0lBRU8sK0NBQXlCLEdBQWpDLFVBQWtDLE1BQXVCO1FBRXJELE1BQU0sQ0FBQyxJQUFJLENBQUMsRUFBRSxDQUFDLEtBQUssQ0FBQyx1T0FJaEIsRUFBRSxDQUFDLE1BQU0sQ0FBQyxDQUFDO2FBQ1gsSUFBSSxDQUFDLFVBQUMsT0FBYyxJQUFLLE9BQUEsTUFBTSxDQUFDLE9BQU8sQ0FBQyxDQUFDLENBQUMsQ0FBQyxDQUFDLENBQUMsQ0FBQyxLQUFLLENBQUMsQ0FBQyxJQUFJLENBQUMsRUFBakMsQ0FBaUMsQ0FBQyxDQUFDO0lBQ3JFLENBQUM7SUFFTSw2Q0FBdUIsR0FBOUIsVUFBK0IsTUFBcUI7UUFBcEQsaUJBSUM7UUFIRyxNQUFNLENBQUMsSUFBSSxDQUFDLCtCQUErQixDQUFDLE1BQU0sQ0FBQyxDQUFDLElBQUksQ0FBQyxVQUFBLFlBQVk7WUFDakUsTUFBTSxDQUFDLEtBQUksQ0FBQywyQkFBMkIsQ0FBQyxZQUFZLENBQUMsQ0FBQTtRQUN6RCxDQUFDLENBQUMsQ0FBQztJQUNQLENBQUM7SUFFTyw2Q0FBdUIsR0FBL0IsVUFBZ0MsTUFBcUI7UUFLakQsTUFBTSxDQUFDLElBQUksQ0FBQyxFQUFFLENBQUMsS0FBSyxDQUFDLHFSQU1oQixFQUFFLENBQUMsTUFBTSxDQUFDLENBQUM7YUFDWCxJQUFJLENBQUMsVUFBQSxPQUFPLElBQUksT0FBQSxPQUFPLENBQUMsQ0FBQyxDQUFDLENBQUMsQ0FBQyxDQUFDLEVBQWIsQ0FBYSxDQUFDLENBQUM7SUFDeEMsQ0FBQztJQUVPLGlEQUEyQixHQUFuQyxVQUFvQyxZQUFtQztRQUNuRSxJQUFNLE9BQU8sR0FBRyxFQUFFLEdBQUcsRUFBRSxHQUFHLEVBQUUsR0FBRyxJQUFJLENBQUM7UUFDcEMsSUFBTSxZQUFZLEdBQUcsSUFBSSxJQUFJLENBQUMsSUFBSSxDQUFDLEdBQUcsRUFBRSxHQUFHLE9BQU8sQ0FBQyxDQUFDO1FBRXBELElBQUksV0FBVyxHQUFHLENBQUMsQ0FBQztRQUNwQixJQUFJLFdBQVcsR0FBRyxDQUFDLENBQUM7UUFDcEIsSUFBSSxRQUFRLEdBQUcsQ0FBQyxDQUFDO1FBQ2pCLElBQUksTUFBTSxHQUFHLENBQUMsQ0FBQztRQUNmLElBQUksbUJBQW1CLEdBQUcsQ0FBQyxDQUFDO1FBQzVCLElBQUksb0JBQW9CLEdBQUcsQ0FBQyxDQUFDO1FBRTdCLElBQUksTUFBYyxDQUFDO1FBQ25CLElBQUksV0FBaUIsQ0FBQztRQUV0QixHQUFHLENBQUMsQ0FBc0IsVUFBWSxFQUFaLDZCQUFZLEVBQVosMEJBQVksRUFBWixJQUFZO1lBQWpDLElBQU0sV0FBVyxxQkFBQTtZQUNsQixNQUFNLEdBQUcsV0FBVyxDQUFDLE1BQU0sQ0FBQztZQUM1QixNQUFNLENBQUMsQ0FBQyxXQUFXLENBQUMsZ0JBQWdCLENBQUMsQ0FBQyxDQUFDO2dCQUNuQyxLQUFLLFNBQVM7b0JBQ1YsV0FBVyxJQUFJLE1BQU0sQ0FBQztvQkFDdEIsV0FBVyxHQUFHLElBQUksSUFBSSxDQUFDLFdBQVcsQ0FBQyxZQUFZLENBQUMsQ0FBQztvQkFDakQsRUFBRSxDQUFDLENBQUMsV0FBVyxDQUFDLFNBQVMsSUFBSSxHQUFHO3dCQUM1QixXQUFXLEdBQUcsWUFBWSxDQUFDO3dCQUMzQixNQUFNLElBQUksTUFBTSxDQUFDO29CQUNyQixLQUFLLENBQUM7Z0JBQ1YsS0FBSyxZQUFZO29CQUNiLFdBQVcsSUFBSSxNQUFNLENBQUM7b0JBQ3RCLEtBQUssQ0FBQztnQkFDVixLQUFLLFNBQVM7b0JBQ1YsRUFBRSxDQUFDLENBQUMsV0FBVyxDQUFDLFNBQVMsSUFBSSxHQUFHLENBQUM7d0JBQzdCLEtBQUssQ0FBQztvQkFDVixNQUFNLENBQUMsQ0FBQyxXQUFXLENBQUMsWUFBWSxDQUFDLENBQUMsQ0FBQzt3QkFDL0IsS0FBSyxXQUFXOzRCQUNaLEVBQUUsQ0FBQyxDQUFDLE1BQU0sR0FBRyxDQUFDLENBQUM7Z0NBQUMsb0JBQW9CLElBQUksTUFBTSxDQUFDOzRCQUMvQyxLQUFLLENBQUM7d0JBQ1Y7NEJBQ0ksRUFBRSxDQUFDLENBQUMsTUFBTSxHQUFHLENBQUMsQ0FBQztnQ0FBQyxRQUFRLElBQUksTUFBTSxDQUFDOzRCQUNuQyxFQUFFLENBQUMsQ0FBQyxNQUFNLEdBQUcsQ0FBQyxDQUFDO2dDQUFDLG1CQUFtQixJQUFJLE1BQU0sQ0FBQzs0QkFDOUMsS0FBSyxDQUFDO29CQUNkLENBQUM7b0JBQ0QsS0FBSyxDQUFBO1lBQ2IsQ0FBQztTQUNKO1FBRUQsSUFBTSxPQUFPLEdBQUcsV0FBVyxHQUFHLFFBQVEsR0FBRyxXQUFXLENBQUM7UUFDckQsSUFBTSxTQUFTLEdBQUcsV0FBVyxHQUFHLG9CQUFvQixDQUFDO1FBQ3JELE1BQU0sQ0FBQztZQUNILFdBQVcsRUFBRSxXQUFXO1lBQ3hCLFFBQVEsRUFBRSxRQUFRO1lBRWxCLFdBQVcsRUFBRSxXQUFXO1lBQ3hCLG9CQUFvQixFQUFFLG9CQUFvQjtZQUMxQyxtQkFBbUIsRUFBRSxtQkFBbUI7WUFFeEMsT0FBTyxFQUFFLE9BQU87WUFDaEIsaUJBQWlCLEVBQUUsTUFBTTtZQUN6QixnQkFBZ0IsRUFBRSxPQUFPLEdBQUcsTUFBTTtZQUNsQyxpQkFBaUIsRUFBRSxRQUFRLEdBQUcsbUJBQW1CO1lBQ2pELGtCQUFrQixFQUFFLFNBQVMsR0FBRyxNQUFNO1lBRXRDLFNBQVMsRUFBRSxTQUFTO1NBQ3ZCLENBQUM7SUFDTixDQUFDO0lBRU8scURBQStCLEdBQXZDLFVBQXdDLE1BQXFCO1FBQ3pELE1BQU0sQ0FBQyxJQUFJLENBQUMsRUFBRSxDQUFDLEtBQUssQ0FBQyx3WUFNcEIsRUFBRSxDQUFDLE1BQU0sQ0FBQyxDQUFDLENBQUMsSUFBSSxDQUFDLFVBQUEsT0FBTyxJQUFJLE9BQUEsT0FBTyxDQUFDLENBQUMsQ0FBQyxFQUFWLENBQVUsQ0FBQyxDQUFDO0lBQzdDLENBQUM7SUExWEQ7UUFEQyx5QkFBUyxDQUFDLENBQUMsTUFBTSxFQUFFLE9BQU8sQ0FBQyxDQUFDOzs7O21EQThDNUI7SUFHRDtRQURDLHlCQUFTLENBQUMsQ0FBQyxNQUFNLEVBQUUsT0FBTyxDQUFDLENBQUM7Ozs7aURBSTVCO0lBR0Q7UUFEQyx5QkFBUyxDQUFDLENBQUMsTUFBTSxFQUFFLE9BQU8sQ0FBQyxDQUFDOzs7O2dEQUc1QjtJQUdEO1FBREMseUJBQVMsQ0FBQyxDQUFDLE1BQU0sRUFBRSxPQUFPLENBQUMsQ0FBQzs7OzttREFxRzVCO0lBN05RLFdBQVc7UUFEdkIsd0JBQVE7T0FDSSxXQUFXLENBeWJ2QjtJQUFELGtCQUFDO0NBQUEsQUF6YkQsSUF5YkM7QUF6Ylksa0NBQVciLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgKiBhcyBFeHByZXNzIGZyb20gXCJleHByZXNzXCI7XG5pbXBvcnQgKiBhcyBQcm9taXNlIGZyb20gXCJibHVlYmlyZFwiO1xuaW1wb3J0ICogYXMgVXRpbHMgZnJvbSBcIi4uLy4uL2NvbXBvbmVudHMvVXRpbHNcIjtcbmltcG9ydCAqIGFzIEh0dHBTdGF0dXMgZnJvbSBcImh0dHAtc3RhdHVzLWNvZGVzXCI7XG5pbXBvcnQge0lNb2R1bGUsIEF1dGhvcml6ZSwgUmVnaXN0ZXJ9IGZyb20gXCIuLi9EZWZhdWx0TW9kdWxlXCI7XG5jb25zdCBCbG9ja1RyYWlsOiBhbnkgPSByZXF1aXJlKCdibG9ja3RyYWlsLXNkaycpO1xuY29uc3QgTUlOX1dJVEhEUkFXID0gQmxvY2tUcmFpbC50b1NhdG9zaGkoMC4wMDAxKTtcblxuQFJlZ2lzdGVyXG5leHBvcnQgY2xhc3MgTG9naW5Nb2R1bGUgaW1wbGVtZW50cyBJTW9kdWxlIHtcbiAgICBwdWJsaWMgbmFtZTogc3RyaW5nID0gXCJsb2dpblwiO1xuICAgIHB1YmxpYyB1cmw6IHN0cmluZyA9IFwiL3VzZXJcIjtcbiAgICBwdWJsaWMgY29uZmlnOiBhbnk7XG4gICAgcHVibGljIGRiOiBhbnk7XG4gICAgcHVibGljIGJsb2NrdHJhaWw6IGFueTtcblxuICAgIHB1YmxpYyBpbml0KCkge307XG5cbiAgICBwdWJsaWMgZ2V0Um91dGUoKSB7XG4gICAgICAgIGxldCByb3V0ZXIgPSBFeHByZXNzLlJvdXRlcigpO1xuXG4gICAgICAgIHJvdXRlci5wb3N0KFwiL2xvZ2luXCIsIHRoaXMubWFrZUxvZ2luLmJpbmQodGhpcykpO1xuICAgICAgICByb3V0ZXIucG9zdChcIi9zaWdudXBcIiwgdGhpcy5tYWtlU2lnbnVwLmJpbmQodGhpcykpO1xuICAgICAgICByb3V0ZXIuZ2V0KFwiL2xvZ291dFwiLCB0aGlzLm1ha2VMb2dvdXQuYmluZCh0aGlzKSk7XG4gICAgICAgIHJvdXRlci5nZXQoXCIvY2hlY2tcIiwgdGhpcy5jaGVja1VzZXIuYmluZCh0aGlzKSk7XG4gICAgICAgIHJvdXRlci5nZXQoXCIvc3RhdHNcIiwgdGhpcy5nZXRVc2VyU3RhdHMuYmluZCh0aGlzKSk7XG4gICAgICAgIHJvdXRlci5wb3N0KFwiL3dpdGhkcmF3XCIsIHRoaXMubWFrZVdpdGhkcmF3LmJpbmQodGhpcykpO1xuXG4gICAgICAgIHJldHVybiByb3V0ZXI7XG4gICAgfVxuXG4gICAgcHJpdmF0ZSBtYWtlTG9naW4ocmVxdWVzdDogYW55LCByZXNwb25zZTogYW55KSB7XG5cbiAgICAgICAgdGhpcy52ZXJpZnlMb2dpblJlcXVlc3QocmVxdWVzdClcbiAgICAgICAgICAgIC50aGVuKGJvZHkgPT4ge1xuICAgICAgICAgICAgICAgIHJldHVybiB0aGlzLnVzZXJCeVVzZXJuYW1lKGJvZHkudXNlcm5hbWUpXG4gICAgICAgICAgICAgICAgICAgIC50aGVuKHVzZXIgPT4gdGhpcy52ZXJpZnlVc2VyTG9naW4oYm9keSwgdXNlcikpXG4gICAgICAgICAgICAgICAgICAgIC50aGVuKHVzZXIgPT4ge1xuICAgICAgICAgICAgICAgICAgICAgICAgcmVxdWVzdC5zZXNzaW9uLmxvZ2luID0gdXNlcjtcbiAgICAgICAgICAgICAgICAgICAgICAgIFV0aWxzLnJlc3BvbmQuY2FsbChyZXNwb25zZSwgMjAwKTtcbiAgICAgICAgICAgICAgICAgICAgfSlcbiAgICAgICAgICAgICAgICAgICAgLmNhdGNoKChlcnJvcjogYW55KSA9PiB7XG4gICAgICAgICAgICAgICAgICAgICAgICBjb25zb2xlLmxvZyhlcnJvcik7XG4gICAgICAgICAgICAgICAgICAgICAgICBVdGlscy5yZXNwb25kLmNhbGwocmVzcG9uc2UsIDUwMCwgXCJJbnRlcm5hbCBTZXJ2ZXIgRXJyb3JcIik7XG4gICAgICAgICAgICAgICAgICAgIH0pO1xuICAgICAgICAgICAgfSlcbiAgICAgICAgICAgIC5jYXRjaChlcnJvciA9PiB7XG4gICAgICAgICAgICAgICAgVXRpbHMucmVzcG9uZC5jYWxsKHJlc3BvbnNlLCA0MDAsIGVycm9yKTtcbiAgICAgICAgICAgIH0pO1xuICAgIH1cblxuICAgIHByaXZhdGUgbWFrZVNpZ251cChyZXF1ZXN0OiBhbnksIHJlc3BvbnNlOiBhbnkpIHtcbiAgICAgICAgdGhpcy52ZXJpZnlTaWdudXBSZXF1ZXN0KHJlcXVlc3QpXG4gICAgICAgICAgICAudGhlbihib2R5ID0+IHtcbiAgICAgICAgICAgICAgICB0aGlzLmZpbmRXYWxsZXQoYm9keS53YWxsZXQpXG4gICAgICAgICAgICAgICAgICAgIC50aGVuKGFkZHJlc3MgPT4gdGhpcy5jcmVhdGVVc2VyKGJvZHksIGFkZHJlc3MpKVxuICAgICAgICAgICAgICAgICAgICAudGhlbih1c2VyID0+IHtcbiAgICAgICAgICAgICAgICAgICAgICAgIHJlcXVlc3Quc2Vzc2lvbi5sb2dpbiA9IHVzZXI7XG4gICAgICAgICAgICAgICAgICAgICAgICBVdGlscy5yZXNwb25kLmNhbGwocmVzcG9uc2UsIDIwMCk7XG4gICAgICAgICAgICAgICAgICAgIH0pXG4gICAgICAgICAgICAgICAgICAgIC5jYXRjaCgoZXJyb3I6IGFueSkgPT4ge1xuICAgICAgICAgICAgICAgICAgICAgICAgY29uc29sZS5sb2coZXJyb3IpO1xuICAgICAgICAgICAgICAgICAgICAgICAgVXRpbHMucmVzcG9uZC5jYWxsKHJlc3BvbnNlLCA1MDAsIFwiSW50ZXJuYWwgU2VydmVyIEVycm9yXCIpO1xuICAgICAgICAgICAgICAgICAgICB9KTtcbiAgICAgICAgICAgIH0pXG4gICAgICAgICAgICAuY2F0Y2goZXJyb3IgPT4ge1xuICAgICAgICAgICAgICAgIFV0aWxzLnJlc3BvbmQuY2FsbChyZXNwb25zZSwgNDAwLCBlcnJvcik7XG4gICAgICAgICAgICB9KTtcbiAgICB9XG5cbiAgICBAQXV0aG9yaXplKFtcIlVTRVJcIiwgXCJBRE1JTlwiXSlcbiAgICBwcml2YXRlIGdldFVzZXJTdGF0cyhyZXF1ZXN0OiBhbnksIHJlc3BvbnNlOiBhbnkpIHtcbiAgICAgICAgY29uc3QgdXNlcklEID0gcmVxdWVzdC5zZXNzaW9uW1wibG9naW5cIl0uaWQ7XG4gICAgICAgIGxldCBwcm9taXNlcyA9IFtdO1xuXG4gICAgICAgIHByb21pc2VzLnB1c2goXG4gICAgICAgICAgICB0aGlzLnF1ZXJ5VXNlckJhbGFuY2VTdW1tYXJ5KHVzZXJJRCksXG4gICAgICAgICAgICAvLyBwYXlvdXRzICh5b3VyIGxhc3QgMTAgcGF5b3V0cylcbiAgICAgICAgICAgIHRoaXMuZGIucXVlcnkoYFxuICAgICAgICAgICAgICAgIFNFTEVDVCBcXGBhbW91bnRcXGAsIFxcYHRyYW5zYWN0aW9uX2hhc2hcXGBcbiAgICAgICAgICAgICAgICBGUk9NIFxcYHRyYW5zYWN0aW9uc1xcYFxuICAgICAgICAgICAgICAgIFdIRVJFIFxcYHRyYW5zYWN0aW9uX3R5cGVcXGAgPSBcIldJVEhEUkFXQUxcIiBBTkQgXFxgcHJvY2Vzc2VkXFxgID0gXCJZXCJcbiAgICAgICAgICAgICAgICBBTkQgXFxgZGVzY3JpcHRpb25cXGAgTElLRSBcIldpdCVcIiBBTkQgXFxgdXNlcl9pZFxcYCA9ID9cbiAgICAgICAgICAgICAgICBMSU1JVCAxMFxuICAgICAgICAgICAgYCwgW3VzZXJJRF0pLFxuICAgICAgICAgICAgLy8gbGFzdF9yZWYgKHlvdXIgMTAgY29tbWlzc2lvbnMpXG4gICAgICAgICAgICB0aGlzLmRiLnF1ZXJ5KGBcbiAgICAgICAgICAgICAgICBTRUxFQ1QgXFxgYW1vdW50XFxgLCBcXGB0cmFuc2FjdGlvbl9oYXNoXFxgLCBcXGB1c2VybmFtZVxcYFxuICAgICAgICAgICAgICAgIEZST00gXFxgdHJhbnNhY3Rpb25zXFxgXG4gICAgICAgICAgICAgICAgTEVGVCBKT0lOIFxcYGxvZ2luXFxgIE9OIFxcYHRyYW5zYWN0aW9uc1xcYC5cXGB1c2VyX2lkXFxgID0gXFxgbG9naW5cXGAuXFxgaWRcXGBcbiAgICAgICAgICAgICAgICBXSEVSRSBcXGB0cmFuc2FjdGlvbl90eXBlXFxgIElOIChcIldJVEhEUkFXQUxcIixcIlZJUlRVQUxcIikgQU5EIFxcYHByb2Nlc3NlZFxcYCA9ICdZJ1xuICAgICAgICAgICAgICAgIEFORCBcXGBkZXNjcmlwdGlvblxcYCBMSUtFIFwiUmVmJVwiIEFORCBcXGB1c2VyX2lkXFxgID0gP1xuICAgICAgICAgICAgICAgIE9SREVSIEJZIFxcYHByb2Nlc3NfdGltZVxcYCBERVNDXG4gICAgICAgICAgICAgICAgTElNSVQgMTBcbiAgICAgICAgICAgIGAsIFt1c2VySURdKSxcbiAgICAgICAgKTtcblxuICAgICAgICBQcm9taXNlLmFsbChwcm9taXNlcylcbiAgICAgICAgICAgIC50aGVuKChyZXN1bHRzOiBhbnlbXSkgPT4ge1xuICAgICAgICAgICAgICAgIGNvbnN0IHN1bW1hcnkgPSByZXN1bHRzWzBdO1xuICAgICAgICAgICAgICAgIGxldCBzdGF0cyA9IHtcbiAgICAgICAgICAgICAgICAgICAgcGF5b3V0czogcmVzdWx0c1sxXVswXSxcbiAgICAgICAgICAgICAgICAgICAgbGFzdF9yZWY6IHJlc3VsdHNbMl1bMF0sXG4gICAgICAgICAgICAgICAgICAgIGRlcG9zaXRzOiBzdW1tYXJ5LmludmVzdG1lbnRzLFxuICAgICAgICAgICAgICAgICAgICB3aXRoZHJhd2Fsczogc3VtbWFyeS53aXRoZHJhd2FscyxcbiAgICAgICAgICAgICAgICAgICAgcmVmZXJyYWxzOiBzdW1tYXJ5LmVhcm5pbmdzLFxuICAgICAgICAgICAgICAgICAgICBiYWxhbmNlOiBzdW1tYXJ5LmJhbGFuY2UsXG4gICAgICAgICAgICAgICAgICAgIGF2YWlsYWJsZV9iYWxhbmNlOiBzdW1tYXJ5LmF2YWlsYWJsZUJhbGFuY2UsXG4gICAgICAgICAgICAgICAgICAgIGFjdGl2ZV9kZXBvc2l0OiBzdW1tYXJ5LnByaW5jaXBhbCxcbiAgICAgICAgICAgICAgICB9O1xuICAgICAgICAgICAgICAgIFV0aWxzLnJlc3BvbmQuY2FsbChyZXNwb25zZSwgMjAwLCBzdGF0cyk7XG4gICAgICAgICAgICB9KVxuICAgICAgICAgICAgLmNhdGNoKChlcnJvcikgPT4ge1xuICAgICAgICAgICAgICAgIGNvbnNvbGUuZXJyb3IoZXJyb3IpO1xuICAgICAgICAgICAgICAgIFV0aWxzLnJlc3BvbmQuY2FsbChyZXNwb25zZSwgNTAwKTtcbiAgICAgICAgICAgIH0pO1xuICAgIH1cblxuICAgIEBBdXRob3JpemUoW1wiVVNFUlwiLCBcIkFETUlOXCJdKVxuICAgIHByaXZhdGUgbWFrZUxvZ291dChyZXF1ZXN0OiBhbnksIHJlc3BvbnNlOiBhbnkpIHtcbiAgICAgICAgcmVxdWVzdC5zZXNzaW9uLmxvZ2luID0gbnVsbDtcbiAgICAgICAgVXRpbHMucmVzcG9uZC5jYWxsKHJlc3BvbnNlLCAyMDAsIFwiT0tcIik7XG4gICAgfVxuXG4gICAgQEF1dGhvcml6ZShbXCJVU0VSXCIsIFwiQURNSU5cIl0pXG4gICAgcHJpdmF0ZSBjaGVja1VzZXIocmVxdWVzdDogYW55LCByZXNwb25zZTogYW55KSB7XG4gICAgICAgIFV0aWxzLnJlc3BvbmQuY2FsbChyZXNwb25zZSwgMjAwLCByZXF1ZXN0LnNlc3Npb25bXCJsb2dpblwiXSk7XG4gICAgfVxuXG4gICAgQEF1dGhvcml6ZShbXCJVU0VSXCIsIFwiQURNSU5cIl0pXG4gICAgcHJpdmF0ZSBtYWtlV2l0aGRyYXcocmVxdWVzdDogYW55LCByZXNwb25zZTogYW55KSB7XG4gICAgICAgIFByb21pc2UucmVzb2x2ZSgpLnRoZW4oKCkgPT4ge1xuICAgICAgICAgICAgLy8gcHJlcGFyZSBib2R5XG4gICAgICAgICAgICBjb25zdCBib2R5ID0gcmVxdWVzdC5ib2R5IGFzIElXaXRoZHJhd0JvZHk7XG4gICAgICAgICAgICBjb25zdCBhbW91bnQgPSBCbG9ja1RyYWlsLnRvU2F0b3NoaShib2R5LmFtb3VudCk7XG5cbiAgICAgICAgICAgIC8vIHZhbGlkYXRlIGlucHV0XG4gICAgICAgICAgICBpZiAoaXNOYU4oYW1vdW50KSB8fCBhbW91bnQgPCAwKVxuICAgICAgICAgICAgICAgIHJldHVybiBQcm9taXNlLnJlamVjdChcImludmFsaWQgaW5wdXRcIik7XG5cbiAgICAgICAgICAgIC8vIGxpbWl0IGJ5IG1pbiB3aXRoZHJhdyB2YWx1ZVxuICAgICAgICAgICAgaWYgKGFtb3VudCA8IE1JTl9XSVRIRFJBVylcbiAgICAgICAgICAgICAgICByZXR1cm4gUHJvbWlzZS5yZWplY3QoXCJhbW91bnQgaXMgbGVzcyB0aGFuIGxvd2VyIGJvdW5kXCIpO1xuXG4gICAgICAgICAgICBjb25zdCB1c2VySUQgPSByZXF1ZXN0LnNlc3Npb25bXCJsb2dpblwiXS5pZDtcbiAgICAgICAgICAgIHJldHVybiB0aGlzLnF1ZXJ5VXNlckJhbGFuY2VTdW1tYXJ5KHVzZXJJRClcbiAgICAgICAgICAgICAgICAudGhlbihzdW1tYXJ5ID0+IHtcbiAgICAgICAgICAgICAgICAgICAgLy8gbWFrZSBzdXJlIHVzZXIgaGFzIGVub3VnaCB0byB3aXRoZHJhd1xuICAgICAgICAgICAgICAgICAgICBpZiAoYW1vdW50ID4gc3VtbWFyeS5hdmFpbGFibGVCYWxhbmNlKVxuICAgICAgICAgICAgICAgICAgICAgICAgcmV0dXJuIFByb21pc2UucmVqZWN0KFwibm90IGVub3VnaCBmdW5kc1wiKTtcblxuICAgICAgICAgICAgICAgICAgICAvLyBjYWxjdWxhdGUgaG93IG11Y2ggdG8gd2l0aGRyYXcgZnJvbSBlYWNoIGFjY291bnQgdHlwZVxuICAgICAgICAgICAgICAgICAgICBsZXQgZnJvbUVhcm5pbmdzID0gMDtcbiAgICAgICAgICAgICAgICAgICAgbGV0IGZyb21QcmluY2lwYWwgPSAwO1xuICAgICAgICAgICAgICAgICAgICBzd2l0Y2ggKGJvZHkuYWNjb3VudF90eXBlKSB7XG4gICAgICAgICAgICAgICAgICAgICAgICBjYXNlIFwicHJpbmNpcGFsXCI6XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgZnJvbVByaW5jaXBhbCA9IGFtb3VudDtcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICBicmVhaztcbiAgICAgICAgICAgICAgICAgICAgICAgIGNhc2UgXCJlYXJuaW5nc1wiOlxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIGZyb21FYXJuaW5ncyA9IGFtb3VudDtcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICBicmVhaztcbiAgICAgICAgICAgICAgICAgICAgICAgIGRlZmF1bHQ6XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgaWYgKGFtb3VudCA8PSBzdW1tYXJ5LmF2YWlsYWJsZUVhcm5pbmdzKSB7XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIGZyb21FYXJuaW5ncyA9IGFtb3VudDtcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgZWxzZSB7XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIGZyb21FYXJuaW5ncyA9IHN1bW1hcnkuYXZhaWxhYmxlRWFybmluZ3M7XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIGZyb21QcmluY2lwYWwgPSBhbW91bnQgLSBmcm9tRWFybmluZ3M7XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIGJyZWFrXG4gICAgICAgICAgICAgICAgICAgIH1cblxuICAgICAgICAgICAgICAgICAgICAvLyBjaGVjayB3ZSBoYXZlIGVub3VnaCB0byB3aXRoZHJhd1xuICAgICAgICAgICAgICAgICAgICBpZiAoZnJvbVByaW5jaXBhbCA+IHN1bW1hcnkuYXZhaWxhYmxlUHJpbmNpcGFsKVxuICAgICAgICAgICAgICAgICAgICAgICAgcmV0dXJuIFByb21pc2UucmVqZWN0KFwibm90IGVub3VnaCBwcmluY2lwYWxcIik7XG4gICAgICAgICAgICAgICAgICAgIGlmIChmcm9tRWFybmluZ3MgPiBzdW1tYXJ5LmF2YWlsYWJsZUVhcm5pbmdzKVxuICAgICAgICAgICAgICAgICAgICAgICAgcmV0dXJuIFByb21pc2UucmVqZWN0KFwibm90IGVub3VnaCBlYXJuaW5nc1wiKTtcblxuICAgICAgICAgICAgICAgICAgICAvLyBjcmVhdGUgd2l0aGRyYXcgdHJhbnNhY3Rpb25zXG4gICAgICAgICAgICAgICAgICAgIGNvbnNvbGUubG9nKGB3aXRoZHJhdyBhbW91bnQ6ICR7YW1vdW50fSB3aGVyZSBwcmluY2lwYWw6ICR7ZnJvbVByaW5jaXBhbH0gZWFybmluZ3M6ICR7ZnJvbUVhcm5pbmdzfWApO1xuICAgICAgICAgICAgICAgICAgICB0aGlzLnF1ZXJ5VXNlcldhbGxldFdpdGhQbGFuKHVzZXJJRClcbiAgICAgICAgICAgICAgICAgICAgICAgIC50aGVuKGluZm8gPT4ge1xuICAgICAgICAgICAgICAgICAgICAgICAgICAgIGxldCBoYXNoOiBzdHJpbmc7XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgbGV0IGRlc2NyaXB0aW9uOiBzdHJpbmc7XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgY29uc3QgdmFsdWVzID0gW107XG5cbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAvLyBFQVJOSU5HU1xuICAgICAgICAgICAgICAgICAgICAgICAgICAgIGlmIChmcm9tRWFybmluZ3MgPiAwKSB7XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIGxldCB2YWx1ZSA9ICgtZnJvbUVhcm5pbmdzKS50b0ZpeGVkKDApO1xuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBoYXNoID0gYFZJUlRVQUxfJHtpbmZvLnVzZXJfd2FsbGV0fV8ke3VzZXJJRH1fJHt2YWx1ZX1gO1xuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBkZXNjcmlwdGlvbiA9IGBXaXRoZHJhd2FsIEVBUk5JTkdTIGJ5IHVzZXIgJHtCbG9ja1RyYWlsLnRvQlRDKHZhbHVlKX0gQlRDIHRvICR7aW5mby51c2VyX3dhbGxldH0pYDtcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgdmFsdWVzLnB1c2goYChub3coKSwgJ1ZJUlRVQUwnLCAnRUFSTklOR1MnLCAnJHtoYXNofScsIDEsICcke3VzZXJJRH0nLCAnJHt2YWx1ZX0nLCAwLCAnWScsICcke2Rlc2NyaXB0aW9ufScpYCk7XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgfVxuXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgLy8gUFJJTkNJUEFMXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgaWYgKGZyb21QcmluY2lwYWwgPiAwKSB7XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIGxldCB2YWx1ZSA9ICgtZnJvbVByaW5jaXBhbCkudG9GaXhlZCgwKTtcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgaGFzaCA9IGBWSVJUVUFMXyR7aW5mby51c2VyX3dhbGxldH1fJHt1c2VySUR9XyR7dmFsdWV9YDtcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgZGVzY3JpcHRpb24gPSBgV2l0aGRyYXdhbCBQUklOQ0lQQUwgYnkgdXNlciAke0Jsb2NrVHJhaWwudG9CVEModmFsdWUpfSBCVEMgdG8gJHtpbmZvLnVzZXJfd2FsbGV0fSlgO1xuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICB2YWx1ZXMucHVzaChgKG5vdygpLCAnVklSVFVBTCcsICdQUklOQ0lQQUwnLCAnJHtoYXNofScsIDEsICcke3VzZXJJRH0nLCAnJHt2YWx1ZX0nLCAwLCAnWScsICcke2Rlc2NyaXB0aW9ufScpYCk7XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgfVxuXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgLy8gV0lUSERSQVdBTFxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIHtcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgbGV0IHZhbHVlID0gYW1vdW50LnRvRml4ZWQoMCk7XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIGhhc2ggPSBgV0lUSERSQVdBTF8ke2luZm8udXNlcl93YWxsZXR9XyR7dXNlcklEfV8ke3ZhbHVlfWA7XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIGRlc2NyaXB0aW9uID0gYFdpdGhkcmF3YWwgYnkgdXNlciAke0Jsb2NrVHJhaWwudG9CVEModmFsdWUpfSBCVEMgdG8gJHtpbmZvLnVzZXJfd2FsbGV0fSlgO1xuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICB2YWx1ZXMucHVzaChgKG5vdygpLCAnV0lUSERSQVdBTCcsIG51bGwsICcke2hhc2h9JywgMSwgJyR7dXNlcklEfScsICcke3ZhbHVlfScsIDAsICdOJywgJyR7ZGVzY3JpcHRpb259JylgKTtcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICB9XG5cbiAgICAgICAgICAgICAgICAgICAgICAgICAgICByZXR1cm4gdGhpcy5kYi5xdWVyeShgXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBJTlNFUlQgSU5UTyBcXGB0cmFuc2FjdGlvbnNcXGBcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIChcXGBwcm9jZXNzX3RpbWVcXGAsIFxcYHRyYW5zYWN0aW9uX3R5cGVcXGAsIFxcYGFjY291bnRfdHlwZVxcYCwgXFxgdHJhbnNhY3Rpb25faGFzaFxcYCwgXFxgcGF5bWVudF9pZFxcYCwgXFxgdXNlcl9pZFxcYCwgXFxgYW1vdW50XFxgLCBcXGBjb25maXJtYXRpb25zXFxgLCBcXGBwcm9jZXNzZWRcXGAsIFxcYGRlc2NyaXB0aW9uXFxgKVxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgVkFMVUVTXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAke3ZhbHVlcy5qb2luKCcsICcpfVxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBgKTtcbiAgICAgICAgICAgICAgICAgICAgICAgIH0pXG4gICAgICAgICAgICAgICAgICAgICAgICAudGhlbigoKSA9PiB7XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgY29uc29sZS5pbmZvKGB3aXRoZHJhdyBvZiAnJHthbW91bnR9QlRDJyBieSB1c2VyICcke3VzZXJJRH0nIHNjaGVkdWxlZCBzdWNjZXNzZnVsbHlgKTtcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICB0aGlzLnJlc3BvbmQocmVzcG9uc2UsIEh0dHBTdGF0dXMuQUNDRVBURUQpXG4gICAgICAgICAgICAgICAgICAgICAgICB9KVxuICAgICAgICAgICAgICAgICAgICAgICAgLmNhdGNoKGUgPT4ge1xuICAgICAgICAgICAgICAgICAgICAgICAgICAgIGNvbnNvbGUuZXJyb3IoXCJlcnJvciB3aGlsZSBjcmVhdGluZyB3aXRoZHJhdyB0cmFuc2FjdGlvblwiLCBlKTtcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICB0aGlzLnJlc3BvbmQocmVzcG9uc2UsIEh0dHBTdGF0dXMuSU5URVJOQUxfU0VSVkVSX0VSUk9SKTtcbiAgICAgICAgICAgICAgICAgICAgICAgIH0pO1xuICAgICAgICAgICAgICAgIH0pO1xuICAgICAgICB9KS5jYXRjaChlID0+IHtcbiAgICAgICAgICAgIGNvbnNvbGUubG9nKFwid2l0aGRyYXcgcmVqZWN0XCIsIGUpO1xuICAgICAgICAgICAgdGhpcy5yZXNwb25kKHJlc3BvbnNlLCBIdHRwU3RhdHVzLkJBRF9SRVFVRVNULCBlKTtcbiAgICAgICAgfSlcbiAgICB9XG5cbiAgICBwcml2YXRlIHZlcmlmeUxvZ2luUmVxdWVzdChyZXF1ZXN0KTogUHJvbWlzZTxJTG9naW5Cb2R5PiB7XG4gICAgICAgIGNvbnN0IGJvZHkgPSByZXF1ZXN0LmJvZHkgYXMgSUxvZ2luQm9keTtcbiAgICAgICAgaWYgKCFib2R5LnVzZXJuYW1lKSByZXR1cm4gUHJvbWlzZS5yZWplY3QoXCJ1c2VybmFtZSBpcyBlbXB0eSFcIik7XG4gICAgICAgIGlmICghYm9keS5wYXNzd29yZCkgcmV0dXJuIFByb21pc2UucmVqZWN0KFwicGFzc3dvcmQgaXMgZW1wdHkhXCIpO1xuICAgICAgICByZXR1cm4gUHJvbWlzZS5yZXNvbHZlKGJvZHkpO1xuICAgIH1cblxuICAgIHByaXZhdGUgdmVyaWZ5U2lnbnVwUmVxdWVzdChyZXF1ZXN0KTogUHJvbWlzZTxJU2lnbnVwQm9keT4ge1xuICAgICAgICBjb25zdCBib2R5ID0gcmVxdWVzdC5ib2R5IGFzIElTaWdudXBCb2R5O1xuICAgICAgICBpZiAoIWJvZHkudXNlcm5hbWUpIHJldHVybiBQcm9taXNlLnJlamVjdChcInVzZXJuYW1lIGlzIGVtcHR5IVwiKTtcbiAgICAgICAgaWYgKCFib2R5LnBhc3N3b3JkKSByZXR1cm4gUHJvbWlzZS5yZWplY3QoXCJwYXNzd29yZCBpcyBlbXB0eSFcIik7XG4gICAgICAgIGlmICghYm9keS5lbWFpbCkgcmV0dXJuIFByb21pc2UucmVqZWN0KFwiZW1haWwgaXMgZW1wdHkhXCIpO1xuICAgICAgICBpZiAoIWJvZHkud2FsbGV0KSByZXR1cm4gUHJvbWlzZS5yZWplY3QoXCJ3YWxsZXQgaXMgZW1wdHkhXCIpO1xuICAgICAgICByZXR1cm4gdGhpcy51c2VyQnlVc2VybmFtZShib2R5LnVzZXJuYW1lKVxuICAgICAgICAgICAgLnRoZW4odXNlciA9PiB1c2VyID8gUHJvbWlzZS5yZWplY3QoXCJ1c2VyIHdpdGggc2FtZSB1c2VybmFtZSBhbHJlYWR5IGV4aXN0IVwiKSA6IG51bGwpXG4gICAgICAgICAgICAudGhlbigoKSA9PiBib2R5KTtcbiAgICB9XG5cbiAgICBwcml2YXRlIGZpbmRXYWxsZXQod2FsbGV0KTogUHJvbWlzZTxhbnk+IHtcbiAgICAgICAgcmV0dXJuIG5ldyBQcm9taXNlKChyZXNvbHZlLCByZWplY3QpID0+IHtcbiAgICAgICAgICAgIHRoaXMuYmxvY2t0cmFpbC5hZGRyZXNzKHdhbGxldCwgKGVycm9yLCBhZGRyZXNzKSA9PiB7XG4gICAgICAgICAgICAgICAgaWYgKGVycm9yKSByZWplY3QoZXJyb3IpO1xuICAgICAgICAgICAgICAgIGVsc2UgaWYgKCFhZGRyZXNzKSByZWplY3QoXCJhZGRyZXNzIGlzIGVtcHR5XCIpO1xuICAgICAgICAgICAgICAgIGVsc2UgcmVzb2x2ZShhZGRyZXNzKTtcbiAgICAgICAgICAgIH0pO1xuICAgICAgICB9KTtcbiAgICB9XG5cbiAgICBwcml2YXRlIHZlcmlmeVVzZXJMb2dpbihib2R5OiBJTG9naW5Cb2R5LCB1c2VyOiBJVXNlcik6IFByb21pc2U8SVVzZXI+IHtcbiAgICAgICAgaWYgKCF1c2VyKVxuICAgICAgICAgICAgcmV0dXJuIFByb21pc2UucmVqZWN0KFwidXNlciBub3QgZXhpc3RcIik7XG5cbiAgICAgICAgLy8gVE9ETzogYmNyeXB0XG4gICAgICAgIGlmICh1c2VyLnBhc3N3b3JkICE9IGJvZHkucGFzc3dvcmQpXG4gICAgICAgICAgICByZXR1cm4gUHJvbWlzZS5yZWplY3QoXCJpbnZhbGlkIHBhc3N3b3JkXCIpO1xuXG4gICAgICAgIHJldHVybiBQcm9taXNlLnJlc29sdmUodXNlcik7XG4gICAgfVxuXG4gICAgcHJpdmF0ZSB1c2VyQnlVc2VybmFtZSh1c2VybmFtZTogc3RyaW5nKTogUHJvbWlzZTxJVXNlcj4ge1xuICAgICAgICByZXR1cm4gdGhpcy5kYi5xdWVyeShcIlNFTEVDVCAqIEZST00gYGxvZ2luYCBXSEVSRSBgdXNlcm5hbWVgID0gP1wiLCBbdXNlcm5hbWVdKVxuICAgICAgICAgICAgLnRoZW4ocm93cyA9PiB7XG4gICAgICAgICAgICAgICAgY29uc3Qgcm93OiBhbnlbXSA9IHJvd3NbMF07XG4gICAgICAgICAgICAgICAgaWYgKHJvdyAmJiByb3cubGVuZ3RoID09IDEpXG4gICAgICAgICAgICAgICAgICAgIHJldHVybiByb3dbMF07XG4gICAgICAgICAgICB9KVxuICAgIH1cblxuICAgIHByaXZhdGUgY3JlYXRlVXNlcihib2R5OiBJU2lnbnVwQm9keSwgYWRkcmVzczogYW55KTogUHJvbWlzZTxJVXNlcj4ge1xuICAgICAgICBjb25zdCByZWZlcnJhbCA9IGJvZHkucmVmZXJyYWwgfHwgbnVsbDtcbiAgICAgICAgcmV0dXJuIHRoaXMuZGIucXVlcnkoXCJJTlNFUlQgSU5UTyBgbG9naW5gIChgZW1haWxgLCBgdXNlcm5hbWVgLCBgcGFzc3dvcmRgLCBgdXNlcl93YWxsZXRgLCBgcmVmZXJyYWxgKVwiICtcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgXCJWQUxVRVMgKD8sID8sID8sID8gLD8pXCIsXG4gICAgICAgICAgICBbYm9keS5lbWFpbCwgYm9keS51c2VybmFtZSwgYm9keS5wYXNzd29yZCwgYWRkcmVzcy5hZGRyZXNzLCByZWZlcnJhbF0pXG4gICAgICAgICAgICAudGhlbihyb3dzID0+IHtcbiAgICAgICAgICAgICAgICByZXR1cm4ge1xuICAgICAgICAgICAgICAgICAgICBpZDogcm93c1swXS5pbnNlcnRJZCxcbiAgICAgICAgICAgICAgICAgICAgdXNlcl93YWxsZXQ6IGFkZHJlc3MuYWRkcmVzcyxcbiAgICAgICAgICAgICAgICAgICAgcG9pbnRzOiAwLFxuICAgICAgICAgICAgICAgICAgICByZWZlcnJhbDogcmVmZXJyYWwsXG4gICAgICAgICAgICAgICAgICAgIHJvbGU6IFwiVVNFUlwiXG4gICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgfSk7XG4gICAgfVxuXG4gICAgcHJpdmF0ZSByZXNwb25kKHJlc3BvbnNlLCBzdGF0dXM6IG51bWJlciwgYm9keT86IGFueSkge1xuICAgICAgICBVdGlscy5yZXNwb25kLmNhbGwocmVzcG9uc2UsIHN0YXR1cywgYm9keSk7XG4gICAgfVxuXG4gICAgcHJpdmF0ZSBxdWVyeVVzZXJJbnZlc3RtZW50c1RvdGFsKHVzZXJJRDogc3RyaW5nIHwgbnVtYmVyLCBwcm9jZXNzZWRPbmx5OiBib29sZWFuID0gdHJ1ZSk6IFByb21pc2U8bnVtYmVyPiB7XG4gICAgICAgIGNvbnN0IGNvbmRpdGlvbiA9IHByb2Nlc3NlZE9ubHkgPyBgQU5EIFxcYHByb2Nlc3NlZFxcYCA9ICdZJ2AgOiAnJztcbiAgICAgICAgY29uc3QgcXVlcnkgPSBgXG4gICAgICAgIFNFTEVDVCBzdW0oXFxgYW1vdW50XFxgKSBhcyBzdW1cbiAgICAgICAgRlJPTSBcXGB0cmFuc2FjdGlvbnNcXGBcbiAgICAgICAgTEVGVCBKT0lOIFxcYHdhbGxldHNcXGAgT04gXFxgdHJhbnNhY3Rpb25zXFxgLlxcYHdhbGxldF9pZFxcYCA9IFxcYHdhbGxldHNcXGAuXFxgaWRcXGBcbiAgICAgICAgTEVGVCBKT0lOIFxcYGxvZ2luXFxgIE9OIFxcYGxvZ2luXFxgLlxcYGlkXFxgID0gXFxgd2FsbGV0c1xcYC5cXGB1c2VyX2lkXFxgXG4gICAgICAgIFdIRVJFIFxcYHRyYW5zYWN0aW9uX3R5cGVcXGAgPSBcIkRFUE9TSVRcIlxuICAgICAgICBBTkQgXFxgY29uZmlybWF0aW9uc1xcYCA+IDBcbiAgICAgICAgJHtjb25kaXRpb259XG4gICAgICAgIEFORCBcXGBsb2dpblxcYC5cXGBpZFxcYCA9ID9cbiAgICAgICAgYDtcbiAgICAgICAgcmV0dXJuIHRoaXMuZGIucXVlcnkocXVlcnksIFt1c2VySURdKVxuICAgICAgICAgICAgLnRoZW4oKHJlc3VsdHM6IGFueVtdKSA9PiBOdW1iZXIocmVzdWx0c1swXVswXVsnc3VtJ10pIHx8IDApO1xuICAgIH1cblxuICAgIC8qKlxuICAgICAqIENhbGN1bGF0ZSBob3cgbXVjaCBvZiB0aGUgZGVwb3NpdCBtb25leSBhcmUgc3RpbGwgZnJvemVuIGFuZCBhcmUgdW5hdmFpbGFibGUgdG8gd2l0aGRyYXcuXG4gICAgICogQHBhcmFtIHVzZXJJRFxuICAgICAqIEBwYXJhbSB0aW1lRGVsdGEgLSBob3cgbXVjaCB0aW1lIG1vbmV5IHJlbWFpbnMgZnJvemVuIGFmdGVyIHRyYW5zZm9ybWF0aW9uIGNvbmZpcm1hdGlvbi5cbiAgICAgKi9cbiAgICBwcml2YXRlIHF1ZXJ5VXNlckZyb3plbkludmVzdG1lbnRzVG90YWwodXNlcklEOiBzdHJpbmcgfCBudW1iZXIsIHRpbWVEZWx0YTogbnVtYmVyID0gMjQgKiA2MCAqIDYwKTogUHJvbWlzZTxudW1iZXI+IHtcbiAgICAgICAgY29uc3QgcXVlcnkgPSBgXG4gICAgICAgIFNFTEVDVCBzdW0oXFxgYW1vdW50XFxgKSBhcyBzdW1cbiAgICAgICAgRlJPTSBcXGB0cmFuc2FjdGlvbnNcXGBcbiAgICAgICAgTEVGVCBKT0lOIFxcYHdhbGxldHNcXGAgT04gXFxgdHJhbnNhY3Rpb25zXFxgLlxcYHdhbGxldF9pZFxcYCA9IFxcYHdhbGxldHNcXGAuXFxgaWRcXGBcbiAgICAgICAgTEVGVCBKT0lOIFxcYGxvZ2luXFxgIE9OIFxcYGxvZ2luXFxgLlxcYGlkXFxgID0gXFxgd2FsbGV0c1xcYC5cXGB1c2VyX2lkXFxgXG4gICAgICAgIFdIRVJFIFxcYHRyYW5zYWN0aW9uX3R5cGVcXGAgPSBcIkRFUE9TSVRcIiBBTkQgXFxgbG9naW5cXGAuXFxgaWRcXGAgPSA/XG4gICAgICAgIEFORCAoKFxcYHRyYW5zYWN0aW9uc1xcYC5cXGBwcm9jZXNzZWRcXGAgIT0gJ1knKSBPUiAoXFxgdHJhbnNhY3Rpb25zXFxgLlxcYHByb2Nlc3NfdGltZVxcYCA+IChOT1coKSAtICR7dGltZURlbHRhfSkpKVxuICAgICAgICBgO1xuICAgICAgICByZXR1cm4gdGhpcy5kYi5xdWVyeShxdWVyeSwgW3VzZXJJRF0pXG4gICAgICAgICAgICAudGhlbigocmVzdWx0czogYW55W10pID0+IE51bWJlcihyZXN1bHRzWzBdWzBdWydzdW0nXSkgfHwgMCk7XG4gICAgfVxuXG4gICAgcHJpdmF0ZSBxdWVyeVVzZXJFYXJuaW5nc1RvdGFsKHVzZXJJRDogc3RyaW5nIHwgbnVtYmVyKTogUHJvbWlzZTxudW1iZXI+IHtcbiAgICAgICAgLy8gVG9EbzogTWFrZSBub3JtYWwgLSBGaXggdGhpcyBzaGl0XG4gICAgICAgIGxldCBxdWVyeSA9IGBcbiAgICAgICAgU0VMRUNUIHN1bShcXGBhbW91bnRcXGApIGFzIHN1bVxuICAgICAgICBGUk9NIFxcYHRyYW5zYWN0aW9uc1xcYFxuICAgICAgICBXSEVSRSAoKFxcYHRyYW5zYWN0aW9uX3R5cGVcXGAgPSBcIldJVEhEUkFXQUxcIiBBTkQgXFxgZGVzY3JpcHRpb25cXGAgTElLRSBcIlJlZiVcIilcbiAgICAgICAgT1IgKFxcYHRyYW5zYWN0aW9uX3R5cGVcXGAgPSBcIlZJUlRVQUxcIikpXG4gICAgICAgIEFORCBcXGBwcm9jZXNzZWRcXGAgPSBcIllcIiBBTkQgXFxgdXNlcl9pZFxcYCA9ID9cbiAgICAgICAgYDtcbiAgICAgICAgcmV0dXJuIHRoaXMuZGIucXVlcnkocXVlcnksIFt1c2VySURdKVxuICAgICAgICAgICAgLnRoZW4oKHJlc3VsdHM6IGFueVtdKSA9PiBOdW1iZXIocmVzdWx0c1swXVswXVsnc3VtJ10pIHx8IDApO1xuICAgIH1cblxuICAgIHByaXZhdGUgcXVlcnlVc2VyV2l0aGRyYXdhbHNUb3RhbCh1c2VySUQ6IHN0cmluZyB8IG51bWJlcik6IFByb21pc2U8bnVtYmVyPiB7XG4gICAgICAgIC8vIFRvRG86IE1ha2Ugbm9ybWFsIC0gRml4IHRoaXMgc2hpdFxuICAgICAgICByZXR1cm4gdGhpcy5kYi5xdWVyeShgXG4gICAgICAgICAgICAgICAgU0VMRUNUIHN1bShcXGBhbW91bnRcXGApIGFzIHN1bVxuICAgICAgICAgICAgICAgIEZST00gXFxgdHJhbnNhY3Rpb25zXFxgXG4gICAgICAgICAgICAgICAgV0hFUkUgXFxgdHJhbnNhY3Rpb25fdHlwZVxcYCA9IFwiV0lUSERSQVdBTFwiIEFORCBcXGBwcm9jZXNzZWRcXGAgPSBcIllcIiBBTkQgXFxgZGVzY3JpcHRpb25cXGAgTElLRSBcIldpdCVcIiBBTkQgXFxgdXNlcl9pZFxcYCA9ID9cbiAgICAgICAgICAgIGAsIFt1c2VySURdKVxuICAgICAgICAgICAgLnRoZW4oKHJlc3VsdHM6IGFueVtdKSA9PiBOdW1iZXIocmVzdWx0c1swXVswXVsnc3VtJ10pIHx8IDApO1xuICAgIH1cblxuICAgIHB1YmxpYyBxdWVyeVVzZXJCYWxhbmNlU3VtbWFyeSh1c2VySUQ6IHN0cmluZ3xudW1iZXIpOiBQcm9taXNlPElVc2VyQmFsYW5jZVN1bW1hcnk+IHtcbiAgICAgICAgcmV0dXJuIHRoaXMuc2VsZWN0Q29tcGFjdFRyYW5zYWN0aW9uc0J5VXNlcih1c2VySUQpLnRoZW4odHJhbnNhY3Rpb25zID0+IHtcbiAgICAgICAgICAgIHJldHVybiB0aGlzLmNhbGN1bGF0ZVVzZXJBY2NvdW50U3VtbWFyeSh0cmFuc2FjdGlvbnMpXG4gICAgICAgIH0pO1xuICAgIH1cblxuICAgIHByaXZhdGUgcXVlcnlVc2VyV2FsbGV0V2l0aFBsYW4odXNlcklEOiBzdHJpbmd8bnVtYmVyKTogUHJvbWlzZTx7XG4gICAgICAgIHVzZXJfd2FsbGV0OiBzdHJpbmcsXG4gICAgICAgIHRyYW5zYWN0aW9uczogbnVtYmVyLFxuICAgICAgICBpbnRlcnZhbDogbnVtYmVyLFxuICAgIH0+IHtcbiAgICAgICAgcmV0dXJuIHRoaXMuZGIucXVlcnkoYFxuICAgICAgICAgICAgU0VMRUNUIFxcYHVzZXJfd2FsbGV0XFxgLCBcXGB0cmFuc2FjdGlvbnNcXGAsIFxcYGludGVydmFsXFxgXG4gICAgICAgICAgICBGUk9NIFxcYGxvZ2luXFxgXG4gICAgICAgICAgICBMRUZUIEpPSU4gXFxgd2FsbGV0c1xcYCBPTiBcXGB3YWxsZXRzXFxgLlxcYHVzZXJfaWRcXGAgPSBcXGBsb2dpblxcYC5cXGBpZFxcYFxuICAgICAgICAgICAgTEVGVCBKT0lOIFxcYHBsYW5zXFxgIE9OIFxcYHBsYW5zXFxgLlxcYGlkXFxgID0gXFxgd2FsbGV0c1xcYC5cXGBwbGFuX2lkXFxgXG4gICAgICAgICAgICBXSEVSRSBcXGB1c2VyX2lkXFxgID0gP1xuICAgICAgICAgICAgYCwgW3VzZXJJRF0pXG4gICAgICAgICAgICAudGhlbihyZXN1bHRzID0+IHJlc3VsdHNbMF1bMF0pO1xuICAgIH1cblxuICAgIHByaXZhdGUgY2FsY3VsYXRlVXNlckFjY291bnRTdW1tYXJ5KHRyYW5zYWN0aW9uczogSUNvbXBhY3RUcmFuc2FjdGlvbltdKTogSVVzZXJCYWxhbmNlU3VtbWFyeSB7XG4gICAgICAgIGNvbnN0IGhvdXJzMjQgPSAyNCAqIDYwICogNjAgKiAxMDAwO1xuICAgICAgICBjb25zdCB1bmZyZWV6ZVRpbWUgPSBuZXcgRGF0ZShEYXRlLm5vdygpIC0gaG91cnMyNCk7XG5cbiAgICAgICAgbGV0IGludmVzdG1lbnRzID0gMDtcbiAgICAgICAgbGV0IHdpdGhkcmF3YWxzID0gMDtcbiAgICAgICAgbGV0IGVhcm5pbmdzID0gMDtcbiAgICAgICAgbGV0IGZyb3plbiA9IDA7XG4gICAgICAgIGxldCBlYXJuaW5nc1dpdGhkcmF3YWxzID0gMDtcbiAgICAgICAgbGV0IHByaW5jaXBhbFdpdGhkcmF3YWxzID0gMDtcblxuICAgICAgICBsZXQgYW1vdW50OiBudW1iZXI7XG4gICAgICAgIGxldCBwcm9jZXNzVGltZTogRGF0ZTtcblxuICAgICAgICBmb3IgKGNvbnN0IHRyYW5zYWN0aW9uIG9mIHRyYW5zYWN0aW9ucykge1xuICAgICAgICAgICAgYW1vdW50ID0gdHJhbnNhY3Rpb24uYW1vdW50O1xuICAgICAgICAgICAgc3dpdGNoICh0cmFuc2FjdGlvbi50cmFuc2FjdGlvbl90eXBlKSB7XG4gICAgICAgICAgICAgICAgY2FzZSAnREVQT1NJVCc6XG4gICAgICAgICAgICAgICAgICAgIGludmVzdG1lbnRzICs9IGFtb3VudDtcbiAgICAgICAgICAgICAgICAgICAgcHJvY2Vzc1RpbWUgPSBuZXcgRGF0ZSh0cmFuc2FjdGlvbi5wcm9jZXNzX3RpbWUpO1xuICAgICAgICAgICAgICAgICAgICBpZiAodHJhbnNhY3Rpb24ucHJvY2Vzc2VkID09ICdOJyB8fFxuICAgICAgICAgICAgICAgICAgICAgICAgcHJvY2Vzc1RpbWUgPiB1bmZyZWV6ZVRpbWUpXG4gICAgICAgICAgICAgICAgICAgICAgICBmcm96ZW4gKz0gYW1vdW50O1xuICAgICAgICAgICAgICAgICAgICBicmVhaztcbiAgICAgICAgICAgICAgICBjYXNlICdXSVRIRFJBV0FMJzpcbiAgICAgICAgICAgICAgICAgICAgd2l0aGRyYXdhbHMgKz0gYW1vdW50O1xuICAgICAgICAgICAgICAgICAgICBicmVhaztcbiAgICAgICAgICAgICAgICBjYXNlICdWSVJUVUFMJzpcbiAgICAgICAgICAgICAgICAgICAgaWYgKHRyYW5zYWN0aW9uLnByb2Nlc3NlZCAhPSAnWScpXG4gICAgICAgICAgICAgICAgICAgICAgICBicmVhaztcbiAgICAgICAgICAgICAgICAgICAgc3dpdGNoICh0cmFuc2FjdGlvbi5hY2NvdW50X3R5cGUpIHtcbiAgICAgICAgICAgICAgICAgICAgICAgIGNhc2UgJ1BSSU5DSVBBTCc6XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgaWYgKGFtb3VudCA8IDApIHByaW5jaXBhbFdpdGhkcmF3YWxzIC09IGFtb3VudDtcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICBicmVhaztcbiAgICAgICAgICAgICAgICAgICAgICAgIGRlZmF1bHQ6XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgaWYgKGFtb3VudCA+IDApIGVhcm5pbmdzICs9IGFtb3VudDtcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICBpZiAoYW1vdW50IDwgMCkgZWFybmluZ3NXaXRoZHJhd2FscyAtPSBhbW91bnQ7XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgYnJlYWs7XG4gICAgICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgICAgICAgICAgYnJlYWtcbiAgICAgICAgICAgIH1cbiAgICAgICAgfVxuXG4gICAgICAgIGNvbnN0IGJhbGFuY2UgPSBpbnZlc3RtZW50cyArIGVhcm5pbmdzIC0gd2l0aGRyYXdhbHM7XG4gICAgICAgIGNvbnN0IHByaW5jaXBhbCA9IGludmVzdG1lbnRzIC0gcHJpbmNpcGFsV2l0aGRyYXdhbHM7XG4gICAgICAgIHJldHVybiB7XG4gICAgICAgICAgICBpbnZlc3RtZW50czogaW52ZXN0bWVudHMsXG4gICAgICAgICAgICBlYXJuaW5nczogZWFybmluZ3MsXG5cbiAgICAgICAgICAgIHdpdGhkcmF3YWxzOiB3aXRoZHJhd2FscyxcbiAgICAgICAgICAgIHByaW5jaXBhbFdpdGhkcmF3YWxzOiBwcmluY2lwYWxXaXRoZHJhd2FscyxcbiAgICAgICAgICAgIGVhcm5pbmdzV2l0aGRyYXdhbHM6IGVhcm5pbmdzV2l0aGRyYXdhbHMsXG5cbiAgICAgICAgICAgIGJhbGFuY2U6IGJhbGFuY2UsXG4gICAgICAgICAgICBmcm96ZW5JbnZlc3RtZW50czogZnJvemVuLFxuICAgICAgICAgICAgYXZhaWxhYmxlQmFsYW5jZTogYmFsYW5jZSAtIGZyb3plbixcbiAgICAgICAgICAgIGF2YWlsYWJsZUVhcm5pbmdzOiBlYXJuaW5ncyAtIGVhcm5pbmdzV2l0aGRyYXdhbHMsXG4gICAgICAgICAgICBhdmFpbGFibGVQcmluY2lwYWw6IHByaW5jaXBhbCAtIGZyb3plbixcblxuICAgICAgICAgICAgcHJpbmNpcGFsOiBwcmluY2lwYWwsXG4gICAgICAgIH07XG4gICAgfVxuXG4gICAgcHJpdmF0ZSBzZWxlY3RDb21wYWN0VHJhbnNhY3Rpb25zQnlVc2VyKHVzZXJJRDogc3RyaW5nfG51bWJlcik6IFByb21pc2U8SUNvbXBhY3RUcmFuc2FjdGlvbltdPiB7XG4gICAgICAgIHJldHVybiB0aGlzLmRiLnF1ZXJ5KGBcbiAgICAgICAgICAgIFNFTEVDVCBESVNUSU5DVCBcXGBwcm9jZXNzX3RpbWVcXGAsIFxcYHRyYW5zYWN0aW9uX3R5cGVcXGAsIFxcYGFjY291bnRfdHlwZVxcYCwgXFxgYW1vdW50XFxgLCBcXGBjb25maXJtYXRpb25zXFxgLCBcXGBwcm9jZXNzZWRcXGBcbiAgICAgICAgICAgIEZST00gXFxgdHJhbnNhY3Rpb25zXFxgXG4gICAgICAgICAgICBMRUZUIEpPSU4gXFxgd2FsbGV0c1xcYCBPTiBcXGB0cmFuc2FjdGlvbnNcXGAuXFxgdXNlcl9pZFxcYCA9IFxcYHdhbGxldHNcXGAuXFxgdXNlcl9pZFxcYCBcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBPUiBcXGB0cmFuc2FjdGlvbnNcXGAuXFxgd2FsbGV0X2lkXFxgID0gXFxgd2FsbGV0c1xcYC5cXGBpZFxcYFxuICAgICAgICAgICAgV0hFUkUgXFxgd2FsbGV0c1xcYC5cXGB1c2VyX2lkXFxgID0gP1xuICAgICAgICBgLCBbdXNlcklEXSkudGhlbihyZXN1bHRzID0+IHJlc3VsdHNbMF0pO1xuICAgIH1cbn1cblxuaW50ZXJmYWNlIElMb2dpbkJvZHkge1xuICAgIHVzZXJuYW1lO1xuICAgIHBhc3N3b3JkO1xufVxuXG5pbnRlcmZhY2UgSVNpZ251cEJvZHkge1xuICAgIHVzZXJuYW1lO1xuICAgIHBhc3N3b3JkO1xuICAgIGVtYWlsO1xuICAgIHdhbGxldDtcbiAgICByZWZlcnJhbDtcbn1cblxuaW50ZXJmYWNlIElVc2VyIHtcbiAgICBpZDogbnVtYmVyO1xuICAgIHVzZXJfd2FsbGVyOiBzdHJpbmc7XG4gICAgcG9pbnRzO1xuICAgIHJlZmVycmFsO1xuICAgIHJvbGU7XG4gICAgZW1haWw7XG4gICAgcGFzc3dvcmQ7XG59XG5cbmludGVyZmFjZSBJV2l0aGRyYXdCb2R5IHtcbiAgICBhbW91bnQ6IG51bWJlcjtcbiAgICBhY2NvdW50X3R5cGU6IFwicHJpbmNpcGFsXCIgfCBcImVhcm5pbmdzXCI7XG59XG5cbmludGVyZmFjZSBJVXNlckJhbGFuY2VTdW1tYXJ5IHtcbiAgICAvKiogU3VtIG9mIHByaW5jaXBhbCBkZXBvc2l0cy4gKi9cbiAgICBpbnZlc3RtZW50czogbnVtYmVyO1xuICAgIC8qKiBTdW0gb2YgYW55IGVhcm5pbmdzIChob3VybHksIHJlZnMsIGV0YykuICovXG4gICAgZWFybmluZ3M6IG51bWJlcjtcblxuICAgIC8qKiBTdW0gb2Ygd2l0aGRyYXdhbHMuICovXG4gICAgd2l0aGRyYXdhbHM6IG51bWJlcjtcbiAgICAvKiogU3VtIG9mIHByaW5jaXBhbCB3aXRoZHJhd2Fscy4gKi9cbiAgICBwcmluY2lwYWxXaXRoZHJhd2FsczogbnVtYmVyO1xuICAgIC8qKiBTdW0gb2YgZWFybmluZ3Mgd2l0aGRyYXdhbHMuICovXG4gICAgZWFybmluZ3NXaXRoZHJhd2FsczogbnVtYmVyO1xuXG4gICAgLyoqIEFjY291bnQgYmFsYW5jZSBpbmNsdWRpbmcgZnJvemVuIGludmVzdG1lbnRzLiAqL1xuICAgIGJhbGFuY2U6IG51bWJlcjtcbiAgICAvKiogQWNjb3VudCBmcm96ZW4gYmFsYW5jZSwgYmFzaWNhbGx5IDI0aCBzaW5jZSBpbnZlc3RtZW50LiAqL1xuICAgIGZyb3plbkludmVzdG1lbnRzOiBudW1iZXI7XG4gICAgLyoqIEhvdyBtdWNoIHVzZXIgY2FuIHdpdGhkcmF3IGluIHRvdGFsIHJpZ2h0IG5vdy4gKi9cbiAgICBhdmFpbGFibGVCYWxhbmNlOiBudW1iZXI7XG4gICAgLyoqIEhvdyBtdWNoIG9mIGVhcm5pbmdzIHVzZXIgY2FuIHdpdGhkcmF3IHJpZ2h0IG5vdy4gKi9cbiAgICBhdmFpbGFibGVFYXJuaW5nczogbnVtYmVyO1xuICAgIC8qKiBIb3cgbXVjaCBvZiBwcmluY2lwYWwgdXNlciBjYW4gd2l0aGRyYXcgcmlnaHQgbm93LiAqL1xuICAgIGF2YWlsYWJsZVByaW5jaXBhbDogbnVtYmVyO1xuXG4gICAgLyoqIFByaW5jaXBhbCBpbnZlc3RtZW50LCB0aGF0IGlzIHVzZWQgdG8gY2FsY3VsYXRlIGludmVzdG1lbnQgcGxhbi4gKi9cbiAgICBwcmluY2lwYWw6IG51bWJlcjtcbn1cblxuaW50ZXJmYWNlIElDb21wYWN0VHJhbnNhY3Rpb24ge1xuICAgIHByb2Nlc3NfdGltZTogRGF0ZSxcbiAgICB0cmFuc2FjdGlvbl90eXBlOiAnREVQT1NJVCcgfCAnVklSVFVBTCcgfCAnV0lUSERSQVdBTCcsXG4gICAgYWNjb3VudF90eXBlOiAnUFJJTkNJUEFMJyB8ICdFQVJOSU5HUycsXG4gICAgYW1vdW50OiBudW1iZXIsXG4gICAgY29uZmlybWF0aW9uczogbnVtYmVyLFxuICAgIHByb2Nlc3NlZDogJ1knIHwgJ04nLFxufVxuIl19