var module = angular
    .module('bitapp', [
        'ui.router',
        'ngMessages',
        'pascalprecht.translate'
    ])
    .config(function ($urlRouterProvider, $qProvider, $stateProvider, $translateProvider) {
        $qProvider.errorOnUnhandledRejections(false);

        if (localStorage && localStorage.getItem("lang")) {
            var language = localStorage.getItem("lang");
            $translateProvider.preferredLanguage(language);
        } else {
            $translateProvider.preferredLanguage('en');
        }

        $translateProvider.registerAvailableLanguageKeys(['en', 'ru', 'cn']);
        $translateProvider.useStaticFilesLoader({
            prefix: 'js/translations/locale-',
            suffix: '.json'
        });

        $stateProvider
            .state("main", {
                abstract: true,
                templateUrl: "js/template/default.html",
                controller: function ($scope, $rootScope, $http, $state) {
                    $scope.makeLogin = function (user) {
                        if (user) {
                            $http.post("/api/user/login", user)
                                .then(function (data) {
                                    $scope.$root.loginPopup = false;
                                    $http.get("/api/user/stats")
                                        .then(function (result) {
                                            $scope.userStats = result.data.message;
                                        })
                                        .catch(function (error) {
                                            console.log(error);
                                        });

                                    $rootScope.user = data.data.message;
                                })
                                .catch(function (error) {
                                    $scope.loginForm["username"].$setValidity('server', false);
                                });
                        }
                    };
                    //====== google map on footer===============//
                    var $googleMaps = jQuery('#map, .page_map');
                    if ($googleMaps.length) {
                        $googleMaps.each(function () {
                            var $map = jQuery(this);
                            var lat;
                            var lng;
                            var map;
                            var styles = [{ "featureType": "administrative", "elementType": "labels.text.fill", "stylers": [{ "color": "#444444" }] }, { "featureType": "landscape", "elementType": "all", "stylers": [{ "color": "#f2f2f2" }] }, { "featureType": "poi", "elementType": "all", "stylers": [{ "visibility": "off" }] }, { "featureType": "road", "elementType": "all", "stylers": [{ "saturation": -100 }, { "lightness": 45 }] }, { "featureType": "road.highway", "elementType": "all", "stylers": [{ "visibility": "simplified" }] }, { "featureType": "road.arterial", "elementType": "labels.icon", "stylers": [{ "visibility": "off" }] }, { "featureType": "transit", "elementType": "all", "stylers": [{ "visibility": "off" }] }, { "featureType": "water", "elementType": "all", "stylers": [{ "color": "#0095b3" }, { "visibility": "on" }] }];
                            var address = $map.data('address') ? $map.data('address') : 'london, baker street, 221b';
                            var markerDescription = $map.find('.map_marker_description').prop('outerHTML');
                            var markerTitle = $map.find('h3').first().text() ? $map.find('h3').first().text() : 'Map Title';
                            var markerIconSrc = $map.find('.map_marker_icon').first().attr('src');
                            jQuery.getJSON('http://maps.googleapis.com/maps/api/geocode/json?address=' + address, function (data) {
                                lat = data.results[0].geometry.location.lat;
                                lng = data.results[0].geometry.location.lng;

                            }).complete(function () {
                                var center = new google.maps.LatLng(lat, lng);
                                var settings = {
                                    mapTypeId: google.maps.MapTypeId.ROADMAP,
                                    zoom: 16,
                                    draggable: true,
                                    scrollwheel: false,
                                    center: center,
                                    styles: styles
                                };
                                map = new google.maps.Map($map[0], settings);
                                var marker = new google.maps.Marker({
                                    position: center,
                                    title: markerTitle,
                                    map: map,
                                    icon: markerIconSrc,
                                });
                                var infowindow = new google.maps.InfoWindow({
                                    content: markerDescription
                                });
                                google.maps.event.addListener(marker, 'click', function () {
                                    infowindow.open(map, marker);
                                });
                            });
                        }); //each
                    }//google map length
                    //======end of google map===================//
                    $scope.makeSignup = function (user) {
                        if (user) {
                            if ($rootScope.referral) {
                                user.referral = $rootScope.referral;
                            }

                            $http.post("/api/user/signup", user)
                                .then(function (data) {
                                    $scope.$root.signupPopup = false;
                                    $scope.signupPopup["wallet"].$setValidity('server', true);
                                    $http.post("/api/plans/create", {"plan_id": 1})
                                        .then(function (result) {
                                            $scope.plan = result.data.message;
                                        })
                                        .catch(function (error) {
                                            console.log(error);
                                        });

                                    $http.get("/api/user/stats")
                                        .then(function (result) {
                                            $scope.userStats = result.data.message;
                                        })
                                        .catch(function (error) {
                                            console.log(error);
                                        });

                                    $rootScope.user = data.data.message;
                                })
                                .catch(function (error) {
                                    $scope.loginForm["wallet"].$setValidity('server', false);
                                });
                        }
                    };

                    $scope.makeLogout = function (user) {
                        $http.get("/api/user/logout", user)
                            .then(function () {
                                $rootScope.user = null;
                            })
                            .catch(function (error) {
                                console.log(error);
                            });
                    }

                    $scope.makeWithdrawPrincipal = function (withdraw) {
                        withdraw.account_type = "principal";
                        $scope.makeWithdraw(withdraw);
                    };

                    $scope.makeWithdrawEarnings = function (withdraw) {
                        withdraw.account_type = "earnings";
                        $scope.makeWithdraw(withdraw);
                    };

                    $scope.makeWithdraw = function (withdraw) {
                        $http.post("/api/user/withdraw", withdraw)
                            .then(function () {
                                console.log("withdraw success!");
                            })
                            .catch(function (error) {
                                $scope.withdrawForm['amount'].$setValidity('server', false);
                                console.log("withdraw error: " + error);
                            });
                    };
                }
            })
            .state("main.about", {
                url: "/about",
                templateUrl: "js/template/about.html",
                controller: function ($scope, $timeout) {
                   
                    $("a[moveto]").click(function (event) {
                        var move = $(this).attr("moveto");
                        if ($(move) && $(move).length) {
                            event.preventDefault();
                            event.stopPropagation();

                            $('html, body').animate({
                                scrollTop: $(move).offset().top
                            }, 200);
                        } else {
                            $timeout(function () {
                                $('html, body').animate({
                                    scrollTop: 0
                                }, 200);
                            }, 200);
                        }
                    });

                    if (window.location.href.indexOf("about") > -1) {
                        jQuery('.toggle_menu').on('click', function () {
                           // console.log("inin");
                            jQuery(this)
                                .toggleClass('mobile-active')
                                    .closest('.page_header')
                                    .toggleClass('mobile-active')
                                    .end()
                                    .closest('.page_toplogo')
                                    .next()
                                    .find('.page_header')
                                    .toggleClass('mobile-active');
                            $(".page_header").toggleClass('mobile-active')
                        });
                    }
                    
                    //======= About Us Employee box equall height========//
                    jQuery('.isotope_container').each(function (index) {
                        console.log("in");
                        var $container = jQuery(this);
                        var layoutMode = ($container.hasClass('masonry-layout')) ? 'masonry' : 'fitRows';
                        var columnWidth = ($container.find('.col-lg-20').length) ? '.col-lg-20' : '';
                        $container.isotope({
                            percentPosition: true,
                            layoutMode: layoutMode,
                            masonry: {
                                //for big first element in grid - giving smaller element to use as grid
                                columnWidth: columnWidth
                            }
                        });

                        var $filters = jQuery(this).attr('data-filters') ? jQuery(jQuery(this).attr('data-filters')) : $container.prev().find('.filters');
                        // bind filter click
                        if ($filters.length) {
                            $filters.on('click', 'a', function (e) {
                                e.preventDefault();
                                var $thisA = jQuery(this);
                                var filterValue = $thisA.attr('data-filter');
                                $container.isotope({ filter: filterValue });
                                $thisA.siblings().removeClass('selected active');
                                $thisA.addClass('selected active');
                            });
                            //for works on select
                            $filters.on('change', 'select', function (e) {
                                e.preventDefault();
                                var filterValue = jQuery(this).val();
                                $container.isotope({ filter: filterValue });
                            });
                        }
                    });
                    //=====End About Us Employee box equall height========//
                    jQuery('.mainmenu a').on('click', function () {
                        var $this = jQuery(this);
                        if (($this.hasClass('sf-with-ul')) || !($this.attr('href').charAt(0) === '#')) {
                            return;
                        }
                        $this
                        .closest('.page_header')
                        .toggleClass('mobile-active')
                        .find('.toggle_menu')
                        .toggleClass('mobile-active');
                        $(".toggle_menu").removeClass('mobile-active')
                        $(".page_header").removeClass('mobile-active')
                    });
                }
            })
             .state("main.faq", {
                 url: "/faq",
                 templateUrl: "js/template/faq.html",
                 controller: function ($scope, $timeout) {                    
                     $(".panel-heading").click(function () {
                         $(this).parent().find(".panel-body").slideToggle();
                         $(this).find("h3").find("a").toggleClass("collapsed");
                     });

                     $("a[moveto]").click(function (event) {
                         var move = $(this).attr("moveto");
                         if ($(move) && $(move).length) {
                             event.preventDefault();
                             event.stopPropagation();

                             $('html, body').animate({
                                 scrollTop: $(move).offset().top
                             }, 200);
                         } else {
                             $timeout(function () {
                                 $('html, body').animate({
                                     scrollTop: 0
                                 }, 200);
                             }, 200);
                         }
                     });                  
                     if (window.location.href.indexOf("faq") > -1) {
                         jQuery('.toggle_menu').on('click', function () {
                            // console.log("inin");
                             jQuery(this)
                                 .toggleClass('mobile-active')
                                     .closest('.page_header')
                                     .toggleClass('mobile-active')
                                     .end()
                                     .closest('.page_toplogo')
                                     .next()
                                     .find('.page_header')
                                     .toggleClass('mobile-active');
                             $(".page_header").toggleClass('mobile-active')
                         });
                     }
                     
                     jQuery('.mainmenu a').on('click', function () {
                         var $this = jQuery(this);
                         if (($this.hasClass('sf-with-ul')) || !($this.attr('href').charAt(0) === '#')) {
                             return;
                         }
                         $this
                         .closest('.page_header')
                         .toggleClass('mobile-active')
                         .find('.toggle_menu')
                         .toggleClass('mobile-active');
                         $(".toggle_menu").removeClass('mobile-active')
                         $(".page_header").removeClass('mobile-active')
                     });
                      
                 }                 
             })
            .state("main.affiliate", {
                url: "/affiliate",
                templateUrl: "js/template/affiliate.html",

                controller: function ($scope, $timeout) {                    
                    $scope.tabs = ['728x90', '468x60', '125x125', '300x600', '250x250']

                    $('.accordion-item h3 a').click(function () {
                        $(this).parent().toggleClass('active');
                        $(this).parent().siblings().slideToggle();
                    });


                    $("a[moveto]").click(function (event) {
                        var move = $(this).attr("moveto");
                        if ($(move) && $(move).length) {
                            event.preventDefault();
                            event.stopPropagation();

                            $('html, body').animate({
                                scrollTop: $(move).offset().top
                            }, 200);
                        } else {
                            $timeout(function () {
                                $('html, body').animate({
                                    scrollTop: 0
                                }, 200);
                            }, 200);
                        }
                    });                 
                    if (window.location.href.indexOf("affiliate") > -1) {
                        jQuery('.toggle_menu').on('click', function () {
                           // console.log("inin");
                            jQuery(this)
                                .toggleClass('mobile-active')
                                    .closest('.page_header')
                                    .toggleClass('mobile-active')
                                    .end()
                                    .closest('.page_toplogo')
                                    .next()
                                    .find('.page_header')
                                    .toggleClass('mobile-active');
                            $(".page_header").toggleClass('mobile-active')
                        });
                    }
                    
                    jQuery('.mainmenu a').on('click', function () {
                        var $this = jQuery(this);
                        if (($this.hasClass('sf-with-ul')) || !($this.attr('href').charAt(0) === '#')) {
                            return;
                        }
                        $this
                        .closest('.page_header')
                        .toggleClass('mobile-active')
                        .find('.toggle_menu')
                        .toggleClass('mobile-active');
                        $(".toggle_menu").removeClass('mobile-active')
                        $(".page_header").removeClass('mobile-active')
                    });
                }

            })
            .state("main.support", {
                url: "/support",
                templateUrl: "js/template/support.html"
            })
            .state("main.terms-of-services", {
                url: "/terms-of-services",
                templateUrl: "js/template/terms-of-Services.html",
                controller: function ($scope, $rootScope, $http, $interval, $timeout) {

                    $("a[moveto]").click(function (event) {
                        var move = $(this).attr("moveto");
                        if ($(move) && $(move).length) {
                            event.preventDefault();
                            event.stopPropagation();

                            $('html, body').animate({ scrollTop: $(move).offset().top }, 200);
                        } else {
                            $timeout(function () {
                                $('html, body').animate({
                                    scrollTop: 0
                                }, 200);
                            }, 200);
                        }
                    });
                    if (window.location.href.indexOf("terms-of-services") > -1) {
                        jQuery('.toggle_menu').on('click', function () {
                            // console.log("inin");
                            jQuery(this)
                                .toggleClass('mobile-active')
                                    .closest('.page_header')
                                    .toggleClass('mobile-active')
                                    .end()
                                    .closest('.page_toplogo')
                                    .next()
                                    .find('.page_header')
                                    .toggleClass('mobile-active');
                            $(".page_header").toggleClass('mobile-active')
                        });
                    }
                   
                    
                    
                    jQuery('.mainmenu a').on('click', function () {
                        var $this = jQuery(this);
                        if (($this.hasClass('sf-with-ul')) || !($this.attr('href').charAt(0) === '#')) {
                            return;
                        }
                        $this
                        .closest('.page_header')
                        .toggleClass('mobile-active')
                        .find('.toggle_menu')
                        .toggleClass('mobile-active');
                        $(".toggle_menu").removeClass('mobile-active')
                        $(".page_header").removeClass('mobile-active')
                    });


                    
                }
                })
    

            .state("main.home", {
                url: "/home",
                templateUrl: "js/template/home.html",
                controller: function ($scope, $rootScope, $http, $interval, $timeout) {
                    $("a[moveto]").click(function (event) {
                        var move = $(this).attr("moveto");
                        if ($(move) && $(move).length) {
                            event.preventDefault();
                            event.stopPropagation();

                            $('html, body').animate({
                                scrollTop: $(move).offset().top
                            }, 200);
                        } else {
                        	$timeout(function () {
	                            $('html, body').animate({
	                                scrollTop: 0
	                            }, 200);
                        	}, 200);
                        }
                    });

                    (function () {
	                    var time = (Date.now() / 1000 | 0);

						$http.get("/api/statistics/totals")
		                        .then(function (data) {
		                            $scope.totals = data.data.message;

                                    try {
                                        var value2 = localStorage.getItem("graph1") && JSON.parse(localStorage.getItem("graph1")) || [{
                                            time: time,
                                            y: $scope.totals.members
                                        }];

    				                    var graph2 = $('#graph1').epoch({
    				                        type: 'time.line',
                                            axes: ['left', 'bottom'],
    				                        fps: 24,
                                            windowSize: 600,
                                            ticks: {
                                                time: 240
                                            },
                                            historySize: 600,
    				                        queueSize: 600,
                                            range: {
                                                left: [0, 300]
                                            },
    				                        data: [{
    				                            label: "Members",
    				                            values: value2
    				                        }]
    				                    });

                                        var value1 = localStorage.getItem("graph2") && JSON.parse(localStorage.getItem("graph2")) || [{
                                            time: time,
                                            y: $scope.totals.deposits / 100000000
                                        }];

                                        var graph1 = $('#graph2').epoch({
                                            type: 'time.line',
                                            axes: ['left', 'bottom'],
                                            fps: 24,
                                            windowSize: 600,
                                            ticks: {
                                                time: 240
                                            },
                                            historySize: 600,
                                            queueSize: 600,
                                            range: {
                                                left: [0, 5]
                                            },
                                            data: [{
                                                label: "Deposit",
                                                values: value1
                                            }]
                                        });

    				                    function getData() {
    					                    $http.get("/api/statistics/totals")
    					                        .then(function (data) {
    					                            $scope.totals = data.data.message;
    					                            setTimeout(getData, 10000);
    					                        })
    					                        .catch(function (error) {
    					                            console.log(error);
    					                        });
    				                    }

    				                    getData();

    				                    window.addEventListener("resize", function () {
                                            graph1.redraw();
    				                    	graph2.redraw();
    				                    });

    				                    var interval = $interval(function () {
                                            time += 1;

    				                        graph2.push([{
    				                            time: time,
    				                            y: $scope.totals.members
    				                        }]);

                                            graph1.push([{
                                                time: time,
                                                y: $scope.totals.deposits / 100000000
                                            }]);

                                            try {
                                                localStorage.setItem("graph2", JSON.stringify(graph1.data[0].values));
                                                localStorage.setItem("graph1", JSON.stringify(graph2.data[0].values));
                                            } catch (e) {
                                                console.log("Can't save to local storage");
                                            }
    				                    }, 1000);

    			                    	$scope.$on("$destroy", function () {
    			                    		$interval.cancel(interval);
    			                    	});
                                    } catch (error) {
                                        console.error(error);
                                    }
		                    	});
                    })();

                    (function () {
                        $http.get("/api/statistics/transactions")
                            .then(function (data) {
                                $scope.statistics = data.data.message;
                                var interval = $interval(function () {

                                $http.get("/api/statistics/transactions")
                                    .then(function (data) {
                                        $scope.statistics = data.data.message;
                                    })
                                    .catch(function (error) {
                                        console.log(error);
                                    });
                                }, 2000);

                                $scope.$on("$destroy", function () {
                                    $interval.cancel(interval);
                                });
                            })
                            .catch(function (error) {
                                console.log(error);
                            });
                    })();


                    //======Index Page Top Slider=======//
                    if (jQuery().flexslider) {
                        var $introSlider = jQuery(".intro_section .flexslider");
                        $introSlider.each(function (index) {
                            var $currentSlider = jQuery(this);
                            var data = $currentSlider.data();
                            var nav = (data.nav !== 'undefined') ? data.nav : true;
                            var dots = (data.dots !== 'undefined') ? data.dots : true;

                            $currentSlider.flexslider({
                                animation: "fade",
                                pauseOnHover: true,
                                useCSS: true,
                                controlNav: dots,
                                directionNav: nav,
                                prevText: "",
                                nextText: "",
                                smoothHeight: false,
                                slideshowSpeed: 10000,
                                animationSpeed: 600,
                                start: function (slider) {
                                    slider.find('.slide_description').children().css({ 'visibility': 'hidden' });
                                    slider.find('.flex-active-slide .slide_description').children().each(function (index) {
                                        var self = jQuery(this);
                                        var animationClass = !self.data('animation') ? 'fadeInRight' : self.data('animation');
                                        setTimeout(function () {
                                            self.addClass("animated " + animationClass);
                                        }, index * 200);
                                    });
                                    slider.find('.flex-control-nav').find('a').each(function () {
                                        jQuery(this).html('0' + jQuery(this).html());
                                    })
                                },
                                after: function (slider) {
                                    slider.find('.flex-active-slide .slide_description').children().each(function (index) {
                                        var self = jQuery(this);
                                        var animationClass = !self.data('animation') ? 'fadeInRight' : self.data('animation');
                                        setTimeout(function () {
                                            self.addClass("animated " + animationClass);
                                        }, index * 200);
                                    });
                                },
                                end: function (slider) {
                                    slider.find('.slide_description').children().each(function () {
                                        var self = jQuery(this);
                                        var animationClass = !self.data('animation') ? 'fadeInRight' : self.data('animation');
                                        self.removeClass('animated ' + animationClass).css({ 'visibility': 'hidden' });
                                        // jQuery(this).attr('class', '');
                                    });
                                },

                            })
                            //wrapping nav with container - uncomment if need
                            .find('.flex-control-nav')
                            .wrap('<div class="container-fluid nav-container"/>')
                        }); //intro_section flex slider

                        jQuery(".flexslider").each(function (index) {
                            var $currentSlider = jQuery(this);
                            //exit if intro slider already activated 
                            if ($currentSlider.find('.flex-active-slide').length) {
                                return;
                            }

                            var data = $currentSlider.data();
                            var nav = (data.nav !== 'undefined') ? data.nav : true;
                            var dots = (data.dots !== 'undefined') ? data.dots : true;
                            var autoplay = (data.autoplay !== 'undefined') ? data.autoplay : true;
                            $currentSlider.flexslider({
                                animation: "slide",
                                useCSS: true,
                                controlNav: dots,
                                directionNav: nav,
                                prevText: "",
                                nextText: "",
                                smoothHeight: false,
                                slideshow: autoplay,
                                slideshowSpeed: 2000,
                                animationSpeed: 400,
                                start: function (slider) {
                                    slider.find('.flex-control-nav').find('a').each(function () {
                                        jQuery(this).html('0' + jQuery(this).html());
                                    })
                                },
                            })
                            .find('.flex-control-nav')
                            .wrap('<div class="container-fluid nav-container"/>')
                        });
                    }
                    //======End Of Slider==============//

                    //===== Coming Soon Counter========//
                    if (jQuery().countdown) {
                        //today date plus month for demo purpose
                        var demoDate = new Date();
                        demoDate.setMonth(demoDate.getMonth() + 1);
                        jQuery('#comingsoon-countdown').countdown({ until: demoDate });
                    }
                    //=====End Of Coming Soon Counter===//

                    //======= Owl Slider Call On Home Page=============//

                    if (jQuery().owlCarousel) {
                        jQuery('#owl-carousel1,#owl-carousel2').each(function () {

                            var $carousel = jQuery(this);
                            var data = $carousel.data();
                           // console.log("in owl carousel");
                            var loop = data.loop ? data.loop : false;
                            var margin = (data.margin || data.margin === 0) ? data.margin : 30;
                            var nav = data.nav ? data.nav : false;
                            var dots = data.dots ? data.dots : false;
                            var themeClass = data.themeclass ? data.themeclass : 'owl-theme';
                            var center = data.center ? data.center : false;
                            var items = data.items ? data.items : 4;
                            var autoplay = data.autoplay ? data.autoplay : false;
                            var responsiveXs = data.responsiveXs ? data.responsiveXs : 1;
                            var responsiveSm = data.responsiveSm ? data.responsiveSm : 2;
                            var responsiveMd = data.responsiveMd ? data.responsiveMd : 3;
                            var responsiveLg = data.responsiveLg ? data.responsiveLg : 4;
                            var responsivexLg = data.responsivexLg ? data.responsivexLg : 6;
                            var filters = data.filters ? data.filters : false;
                            var mouseDrag = $carousel.data('mouse-drag') === false ? false : true;
                            var touchDrag = $carousel.data('touch-drag') === false ? false : true;

                            if (filters) {
                                // $carousel.clone().appendTo($carousel.parent()).addClass( filters.substring(1) + '-carousel-original' );
                                $carousel.after($carousel.clone().addClass('owl-carousel-filter-cloned'));
                                jQuery(filters).on('click', 'a', function (e) {
                                    //processing filter link
                                    e.preventDefault();
                                    var $thisA = jQuery(this);
                                    if ($thisA.hasClass('selected')) {
                                        return;
                                    }
                                    var filterValue = $thisA.attr('data-filter');
                                    $thisA.siblings().removeClass('selected active');
                                    $thisA.addClass('selected active');

                                    //removing old items
                                    $carousel.find('.owl-item').length;
                                    for (var i = $carousel.find('.owl-item').length - 1; i >= 0; i--) {
                                        $carousel.trigger('remove.owl.carousel', [1]);
                                    };

                                    //adding new items
                                    var $filteredItems = jQuery($carousel.next().find(' > ' + filterValue).clone());
                                    $filteredItems.each(function () {
                                        $carousel.trigger('add.owl.carousel', jQuery(this));
                                        jQuery(this).addClass('scaleAppear');
                                    });

                                    $carousel.trigger('refresh.owl.carousel');

                                    //reinit prettyPhoto in filtered OWL carousel
                                    if (jQuery().prettyPhoto) {
                                        $carousel.find("a[data-gal^='prettyPhoto']").prettyPhoto({
                                            hook: 'data-gal',
                                            theme: 'facebook' /* light_rounded / dark_rounded / light_square / dark_square / facebook / pp_default*/
                                        });
                                    }
                                });

                            } //filters

                            $carousel.owlCarousel({
                                loop: loop,
                                margin: margin,
                                nav: nav,
                                autoplay: autoplay,
                                dots: dots,
                                themeClass: themeClass,
                                center: center,
                                items: items,
                                smartSpeed: 400,
                                mouseDrag: mouseDrag,
                                touchDrag: touchDrag,
                                responsive: {
                                    0: {
                                        items: responsiveXs
                                    },
                                    767: {
                                        items: responsiveSm
                                    },
                                    992: {
                                        items: responsiveMd
                                    },
                                    1200: {
                                        items: responsiveLg
                                    }
                                },
                            })
                            .addClass(themeClass);
                            if (center) {
                                $carousel.addClass('owl-center');
                            }

                            //$window.on('resize', function () {
                            //    $carousel.trigger('refresh.owl.carousel');
                            //});

                            $carousel.on('changed.owl.carousel', function () {
                                if (jQuery().prettyPhoto) {
                                    jQuery("a[data-gal^='prettyPhoto']").prettyPhoto({
                                        hook: 'data-gal',
                                        theme: 'facebook', /* light_rounded / dark_rounded / light_square / dark_square / facebook / pp_default*/
                                        social_tools: false
                                    });
                                }
                            })
                        });



                    } //eof owl-carousel

                    //======= End Of Owl Slider Call On Home Page=============//
                    //jQuery('#signupform').on('click', function () {
                         
                    //    console.log("inin");
                    //    $(this).toggleClass("ng-hide");
                    //});
                    //jQuery(".logout").on('click', function () {
                    //    console.log("inin singupbtn");
                    //});

                    $("a[moveto]").click(function (event) {
                        var move = $(this).attr("moveto");
                        if ($(move) && $(move).length) {
                            event.preventDefault();
                            event.stopPropagation();

                            $('html, body').animate({
                                scrollTop: $(move).offset().top
                            }, 100);
                        } else {
                            $timeout(function () {
                                $('html, body').animate({
                                    scrollTop: 0
                                }, 100);
                            }, 200);
                        }
                    });                  
                    if (window.location.href.indexOf("home") > -1) {
                        jQuery('.toggle_menu').on('click', function () {
                           // console.log("inin");
                            jQuery(this)
                                .toggleClass('mobile-active')
                                    .closest('.page_header')
                                    .toggleClass('mobile-active')
                                    .end()
                                    .closest('.page_toplogo')
                                    .next()
                                    .find('.page_header')
                                    .toggleClass('mobile-active');
                            $(".page_header").toggleClass('mobile-active')
                        });
                    }
                    
                    jQuery('.mainmenu a').on('click', function () {
                        var $this = jQuery(this);
                        if (($this.hasClass('sf-with-ul')) || !($this.attr('href').charAt(0) === '#')) {
                            return;
                        }
                        $this
                        .closest('.page_header')
                        .toggleClass('mobile-active')
                        .find('.toggle_menu')
                        .toggleClass('mobile-active');
                        $(".toggle_menu").removeClass('mobile-active')
                        $(".page_header").removeClass('mobile-active')

                    });

                     
                }
            })
            .state("main.page", {
                url: "/page",
                templateUrl: "js/template/page.html",
                controller: function ($scope, $timeout) {
                    //$("a[moveto]").click(function (event) {
                    //    var move = $(this).attr("moveto");
                    //    if ($(move) && $(move).length) {
                    //        event.preventDefault();
                    //        event.stopPropagation();

                    //        $('html, body').animate({
                    //            scrollTop: $(move).offset().top
                    //        }, 200);
                    //    } else {
                    //    	$timeout(function () {
	                //            $('html, body').animate({
	                //                scrollTop: $(move).offset().top
	                //            }, 200);
                    //    	}, 200);
                    //    }
                    //});

                    $scope.tabs = ['728x90', '468x60', '125x125', '300x600', '250x250']

                    $('.accordion-item h3 a').click(function () {
                        $(this).parent().toggleClass('active');
                        $(this).parent().siblings().slideToggle();
                    });
                }
            });

        $urlRouterProvider.otherwise('/home');
    })
    .run(function ($rootScope, $state, $http, $translate) {
        var ref = location.href.match(/referral=([0-9]+)/);

        if (ref) {
            $rootScope.referral = ref[1];
        }

        $rootScope.getTranslation = function () {
            return $translate.proposedLanguage() || $translate.use();
        };

        $rootScope.setTranslation = function (langKey) {
        	if (localStorage && localStorage.setItem) {
        		localStorage.setItem('lang', langKey);
        	}
        	
            $translate.use(langKey);
        };

        $http.get('/api/user/check')
            .then(function (data) {
                $http.post("/api/plans/create", {"plan_id": 1})
                    .then(function (result) {
                        $rootScope.plan = result.data.message;
                    })
                    .catch(function (error) {
                        console.log(error);
                    });

                $http.get("/api/user/stats")
                    .then(function (result) {
                        $rootScope.userStats = result.data.message;
                    })
                    .catch(function (error) {
                        console.log(error);
                    });

                $rootScope.user = data.data.message;
            })
            .catch(function () {
                $state.go('login');
            });
    })
    .name;

angular.element(document).ready(function () {
    angular.bootstrap(document, [module]);
    jQuery('.loginPopup-overflow').on('click', function () {
        console.log("iininin");


    });
});


